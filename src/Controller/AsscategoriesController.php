<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Asscategories Controller
 *
 * @property \App\Model\Table\AsscategoriesTable $Asscategories
 */
class AsscategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('asscategories', $this->paginate($this->Asscategories));
        $this->set('_serialize', ['asscategories']);
    }

    /**
     * View method
     *
     * @param string|null $id Asscategory id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $asscategory = $this->Asscategories->get($id, [
            'contain' => ['Assstrategies']
        ]);
        $this->set('asscategory', $asscategory);
        $this->set('_serialize', ['asscategory']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $asscategory = $this->Asscategories->newEntity();
        if ($this->request->is('post')) {
            $asscategory = $this->Asscategories->patchEntity($asscategory, $this->request->data);
            if ($this->Asscategories->save($asscategory)) {
                $this->Flash->success(__('The asscategory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The asscategory could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('asscategory'));
        $this->set('_serialize', ['asscategory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Asscategory id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $asscategory = $this->Asscategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $asscategory = $this->Asscategories->patchEntity($asscategory, $this->request->data);
            if ($this->Asscategories->save($asscategory)) {
                $this->Flash->success(__('The asscategory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The asscategory could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('asscategory'));
        $this->set('_serialize', ['asscategory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Asscategory id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $asscategory = $this->Asscategories->get($id);
        if ($this->Asscategories->delete($asscategory)) {
            $this->Flash->success(__('The asscategory has been deleted.'));
        } else {
            $this->Flash->error(__('The asscategory could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
