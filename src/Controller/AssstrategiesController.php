<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Assstrategies Controller
 *
 * @property \App\Model\Table\AssstrategiesTable $Assstrategies
 */
class AssstrategiesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Asscategories']
        ];
        $this->set('assstrategies', $this->paginate($this->Assstrategies));
        $this->set('_serialize', ['assstrategies']);
    }

    /**
     * View method
     *
     * @param string|null $id Assstrategy id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $assstrategy = $this->Assstrategies->get($id, [
            'contain' => ['Asscategories', 'Loutcomes']
        ]);
        $this->set('assstrategy', $assstrategy);
        $this->set('_serialize', ['assstrategy']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $assstrategy = $this->Assstrategies->newEntity();
        if ($this->request->is('post')) {
            $assstrategy = $this->Assstrategies->patchEntity($assstrategy, $this->request->data);
            if ($this->Assstrategies->save($assstrategy)) {
                $this->Flash->success(__('The assstrategy has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The assstrategy could not be saved. Please, try again.'));
            }
        }
        $asscategories = $this->Assstrategies->Asscategories->find('list', ['limit' => 200]);
        $loutcomes = $this->Assstrategies->Loutcomes->find('list', ['limit' => 200]);
        $this->set(compact('assstrategy', 'asscategories', 'loutcomes'));
        $this->set('_serialize', ['assstrategy']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Assstrategy id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $assstrategy = $this->Assstrategies->get($id, [
            'contain' => ['Loutcomes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $assstrategy = $this->Assstrategies->patchEntity($assstrategy, $this->request->data);
            if ($this->Assstrategies->save($assstrategy)) {
                $this->Flash->success(__('The assstrategy has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The assstrategy could not be saved. Please, try again.'));
            }
        }
        $asscategories = $this->Assstrategies->Asscategories->find('list', ['limit' => 200]);
        $loutcomes = $this->Assstrategies->Loutcomes->find('list', ['limit' => 200]);
        $this->set(compact('assstrategy', 'asscategories', 'loutcomes'));
        $this->set('_serialize', ['assstrategy']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Assstrategy id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $assstrategy = $this->Assstrategies->get($id);
        if ($this->Assstrategies->delete($assstrategy)) {
            $this->Flash->success(__('The assstrategy has been deleted.'));
        } else {
            $this->Flash->error(__('The assstrategy could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
