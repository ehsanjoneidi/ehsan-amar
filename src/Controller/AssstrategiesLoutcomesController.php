<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AssstrategiesLoutcomes Controller
 *
 * @property \App\Model\Table\AssstrategiesLoutcomesTable $AssstrategiesLoutcomes
 */
class AssstrategiesLoutcomesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Loutcomes', 'Assstrategies']
        ];
        $this->set('assstrategiesLoutcomes', $this->paginate($this->AssstrategiesLoutcomes));
        $this->set('_serialize', ['assstrategiesLoutcomes']);
    }

    /**
     * View method
     *
     * @param string|null $id Assstrategies Loutcome id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $assstrategiesLoutcome = $this->AssstrategiesLoutcomes->get($id, [
            'contain' => ['Loutcomes', 'Assstrategies']
        ]);
        $this->set('assstrategiesLoutcome', $assstrategiesLoutcome);
        $this->set('_serialize', ['assstrategiesLoutcome']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $assstrategiesLoutcome = $this->AssstrategiesLoutcomes->newEntity();
        if ($this->request->is('post')) {
            $assstrategiesLoutcome = $this->AssstrategiesLoutcomes->patchEntity($assstrategiesLoutcome, $this->request->data);
            if ($this->AssstrategiesLoutcomes->save($assstrategiesLoutcome)) {
                $this->Flash->success(__('The assstrategies loutcome has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The assstrategies loutcome could not be saved. Please, try again.'));
            }
        }
        $loutcomes = $this->AssstrategiesLoutcomes->Loutcomes->find('list', ['limit' => 200]);
        $assstrategies = $this->AssstrategiesLoutcomes->Assstrategies->find('list', ['limit' => 200]);
        $this->set(compact('assstrategiesLoutcome', 'loutcomes', 'assstrategies'));
        $this->set('_serialize', ['assstrategiesLoutcome']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Assstrategies Loutcome id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $assstrategiesLoutcome = $this->AssstrategiesLoutcomes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $assstrategiesLoutcome = $this->AssstrategiesLoutcomes->patchEntity($assstrategiesLoutcome, $this->request->data);
            if ($this->AssstrategiesLoutcomes->save($assstrategiesLoutcome)) {
                $this->Flash->success(__('The assstrategies loutcome has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The assstrategies loutcome could not be saved. Please, try again.'));
            }
        }
        $loutcomes = $this->AssstrategiesLoutcomes->Loutcomes->find('list', ['limit' => 200]);
        $assstrategies = $this->AssstrategiesLoutcomes->Assstrategies->find('list', ['limit' => 200]);
        $this->set(compact('assstrategiesLoutcome', 'loutcomes', 'assstrategies'));
        $this->set('_serialize', ['assstrategiesLoutcome']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Assstrategies Loutcome id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $assstrategiesLoutcome = $this->AssstrategiesLoutcomes->get($id);
        if ($this->AssstrategiesLoutcomes->delete($assstrategiesLoutcome)) {
            $this->Flash->success(__('The assstrategies loutcome has been deleted.'));
        } else {
            $this->Flash->error(__('The assstrategies loutcome could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
