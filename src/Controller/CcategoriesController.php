<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ccategories Controller
 *
 * @property \App\Model\Table\CcategoriesTable $Ccategories
 */
class CcategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('ccategories', $this->paginate($this->Ccategories));
        $this->set('_serialize', ['ccategories']);
    }

    /**
     * View method
     *
     * @param string|null $id Ccategory id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ccategory = $this->Ccategories->get($id, [
            'contain' => ['Dcourses']
        ]);
        $this->set('ccategory', $ccategory);
        $this->set('_serialize', ['ccategory']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ccategory = $this->Ccategories->newEntity();
        if ($this->request->is('post')) {
            $ccategory = $this->Ccategories->patchEntity($ccategory, $this->request->data);
            if ($this->Ccategories->save($ccategory)) {
                $this->Flash->success(__('The ccategory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ccategory could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ccategory'));
        $this->set('_serialize', ['ccategory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ccategory id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ccategory = $this->Ccategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ccategory = $this->Ccategories->patchEntity($ccategory, $this->request->data);
            if ($this->Ccategories->save($ccategory)) {
                $this->Flash->success(__('The ccategory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ccategory could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ccategory'));
        $this->set('_serialize', ['ccategory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ccategory id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ccategory = $this->Ccategories->get($id);
        if ($this->Ccategories->delete($ccategory)) {
            $this->Flash->success(__('The ccategory has been deleted.'));
        } else {
            $this->Flash->error(__('The ccategory could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
