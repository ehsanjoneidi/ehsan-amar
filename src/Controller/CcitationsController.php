<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ccitations Controller
 *
 * @property \App\Model\Table\CcitationsTable $Ccitations
 */
class CcitationsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Rcategories']
        ];
        $this->set('ccitations', $this->paginate($this->Ccitations));
        $this->set('_serialize', ['ccitations']);
    }

    /**
     * View method
     *
     * @param string|null $id Ccitation id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ccitation = $this->Ccitations->get($id, [
            'contain' => ['Rcategories', 'Ccontents']
        ]);
        $this->set('ccitation', $ccitation);
        $this->set('_serialize', ['ccitation']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ccitation = $this->Ccitations->newEntity();
        if ($this->request->is('post')) {
            $ccitation = $this->Ccitations->patchEntity($ccitation, $this->request->data);
            if ($this->Ccitations->save($ccitation)) {
                $this->Flash->success(__('The ccitation has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ccitation could not be saved. Please, try again.'));
            }
        }
        $rcategories = $this->Ccitations->Rcategories->find('list', ['limit' => 200]);
        $ccontents = $this->Ccitations->Ccontents->find('list', ['limit' => 200]);
        $this->set(compact('ccitation', 'rcategories', 'ccontents'));
        $this->set('_serialize', ['ccitation']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ccitation id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ccitation = $this->Ccitations->get($id, [
            'contain' => ['Ccontents']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ccitation = $this->Ccitations->patchEntity($ccitation, $this->request->data);
            if ($this->Ccitations->save($ccitation)) {
                $this->Flash->success(__('The ccitation has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ccitation could not be saved. Please, try again.'));
            }
        }
        $rcategories = $this->Ccitations->Rcategories->find('list', ['limit' => 200]);
        $ccontents = $this->Ccitations->Ccontents->find('list', ['limit' => 200]);
        $this->set(compact('ccitation', 'rcategories', 'ccontents'));
        $this->set('_serialize', ['ccitation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ccitation id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ccitation = $this->Ccitations->get($id);
        if ($this->Ccitations->delete($ccitation)) {
            $this->Flash->success(__('The ccitation has been deleted.'));
        } else {
            $this->Flash->error(__('The ccitation could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
