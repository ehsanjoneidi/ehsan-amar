<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CcontentsCcitations Controller
 *
 * @property \App\Model\Table\CcontentsCcitationsTable $CcontentsCcitations
 */
class CcontentsCcitationsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ccontents', 'Ccitations']
        ];
        $this->set('ccontentsCcitations', $this->paginate($this->CcontentsCcitations));
        $this->set('_serialize', ['ccontentsCcitations']);
    }

    /**
     * View method
     *
     * @param string|null $id Ccontents Ccitation id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ccontentsCcitation = $this->CcontentsCcitations->get($id, [
            'contain' => ['Ccontents', 'Ccitations']
        ]);
        $this->set('ccontentsCcitation', $ccontentsCcitation);
        $this->set('_serialize', ['ccontentsCcitation']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ccontentsCcitation = $this->CcontentsCcitations->newEntity();
        if ($this->request->is('post')) {
            $ccontentsCcitation = $this->CcontentsCcitations->patchEntity($ccontentsCcitation, $this->request->data);
            if ($this->CcontentsCcitations->save($ccontentsCcitation)) {
                $this->Flash->success(__('The ccontents ccitation has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ccontents ccitation could not be saved. Please, try again.'));
            }
        }
        $ccontents = $this->CcontentsCcitations->Ccontents->find('list', ['limit' => 200]);
        $ccitations = $this->CcontentsCcitations->Ccitations->find('list', ['limit' => 200]);
        $this->set(compact('ccontentsCcitation', 'ccontents', 'ccitations'));
        $this->set('_serialize', ['ccontentsCcitation']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ccontents Ccitation id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ccontentsCcitation = $this->CcontentsCcitations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ccontentsCcitation = $this->CcontentsCcitations->patchEntity($ccontentsCcitation, $this->request->data);
            if ($this->CcontentsCcitations->save($ccontentsCcitation)) {
                $this->Flash->success(__('The ccontents ccitation has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ccontents ccitation could not be saved. Please, try again.'));
            }
        }
        $ccontents = $this->CcontentsCcitations->Ccontents->find('list', ['limit' => 200]);
        $ccitations = $this->CcontentsCcitations->Ccitations->find('list', ['limit' => 200]);
        $this->set(compact('ccontentsCcitation', 'ccontents', 'ccitations'));
        $this->set('_serialize', ['ccontentsCcitation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ccontents Ccitation id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ccontentsCcitation = $this->CcontentsCcitations->get($id);
        if ($this->CcontentsCcitations->delete($ccontentsCcitation)) {
            $this->Flash->success(__('The ccontents ccitation has been deleted.'));
        } else {
            $this->Flash->error(__('The ccontents ccitation could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
