<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ccontents Controller
 *
 * @property \App\Model\Table\CcontentsTable $Ccontents
 */
class CcontentsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('ccontents', $this->paginate($this->Ccontents));
        $this->set('_serialize', ['ccontents']);
    }

    /**
     * View method
     *
     * @param string|null $id Ccontent id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ccontent = $this->Ccontents->get($id, [
            'contain' => ['Ccitations', 'Dunits', 'Topics']
        ]);
        $this->set('ccontent', $ccontent);
        $this->set('_serialize', ['ccontent']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ccontent = $this->Ccontents->newEntity();
        if ($this->request->is('post')) {
            $ccontent = $this->Ccontents->patchEntity($ccontent, $this->request->data);
            if ($this->Ccontents->save($ccontent)) {
                $this->Flash->success(__('The ccontent has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ccontent could not be saved. Please, try again.'));
            }
        }
        $ccitations = $this->Ccontents->Ccitations->find('list', ['limit' => 200]);
        $dunits = $this->Ccontents->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('ccontent', 'ccitations', 'dunits'));
        $this->set('_serialize', ['ccontent']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ccontent id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ccontent = $this->Ccontents->get($id, [
            'contain' => ['Ccitations', 'Dunits']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ccontent = $this->Ccontents->patchEntity($ccontent, $this->request->data);
            if ($this->Ccontents->save($ccontent)) {
                $this->Flash->success(__('The ccontent has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ccontent could not be saved. Please, try again.'));
            }
        }
        $ccitations = $this->Ccontents->Ccitations->find('list', ['limit' => 200]);
        $dunits = $this->Ccontents->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('ccontent', 'ccitations', 'dunits'));
        $this->set('_serialize', ['ccontent']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ccontent id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ccontent = $this->Ccontents->get($id);
        if ($this->Ccontents->delete($ccontent)) {
            $this->Flash->success(__('The ccontent has been deleted.'));
        } else {
            $this->Flash->error(__('The ccontent could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
