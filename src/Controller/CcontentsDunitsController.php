<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CcontentsDunits Controller
 *
 * @property \App\Model\Table\CcontentsDunitsTable $CcontentsDunits
 */
class CcontentsDunitsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ccontents', 'Dunits']
        ];
        $this->set('ccontentsDunits', $this->paginate($this->CcontentsDunits));
        $this->set('_serialize', ['ccontentsDunits']);
    }

    /**
     * View method
     *
     * @param string|null $id Ccontents Dunit id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ccontentsDunit = $this->CcontentsDunits->get($id, [
            'contain' => ['Ccontents', 'Dunits']
        ]);
        $this->set('ccontentsDunit', $ccontentsDunit);
        $this->set('_serialize', ['ccontentsDunit']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ccontentsDunit = $this->CcontentsDunits->newEntity();
        if ($this->request->is('post')) {
            $ccontentsDunit = $this->CcontentsDunits->patchEntity($ccontentsDunit, $this->request->data);
            if ($this->CcontentsDunits->save($ccontentsDunit)) {
                $this->Flash->success(__('The ccontents dunit has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ccontents dunit could not be saved. Please, try again.'));
            }
        }
        $ccontents = $this->CcontentsDunits->Ccontents->find('list', ['limit' => 200]);
        $dunits = $this->CcontentsDunits->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('ccontentsDunit', 'ccontents', 'dunits'));
        $this->set('_serialize', ['ccontentsDunit']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ccontents Dunit id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ccontentsDunit = $this->CcontentsDunits->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ccontentsDunit = $this->CcontentsDunits->patchEntity($ccontentsDunit, $this->request->data);
            if ($this->CcontentsDunits->save($ccontentsDunit)) {
                $this->Flash->success(__('The ccontents dunit has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ccontents dunit could not be saved. Please, try again.'));
            }
        }
        $ccontents = $this->CcontentsDunits->Ccontents->find('list', ['limit' => 200]);
        $dunits = $this->CcontentsDunits->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('ccontentsDunit', 'ccontents', 'dunits'));
        $this->set('_serialize', ['ccontentsDunit']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ccontents Dunit id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ccontentsDunit = $this->CcontentsDunits->get($id);
        if ($this->CcontentsDunits->delete($ccontentsDunit)) {
            $this->Flash->success(__('The ccontents dunit has been deleted.'));
        } else {
            $this->Flash->error(__('The ccontents dunit could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
