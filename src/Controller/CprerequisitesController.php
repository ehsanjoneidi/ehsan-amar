<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cprerequisites Controller
 *
 * @property \App\Model\Table\CprerequisitesTable $Cprerequisites
 */
class CprerequisitesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ucategories']
        ];
        $this->set('cprerequisites', $this->paginate($this->Cprerequisites));
        $this->set('_serialize', ['cprerequisites']);
    }

    /**
     * View method
     *
     * @param string|null $id Cprerequisite id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cprerequisite = $this->Cprerequisites->get($id, [
            'contain' => ['Ucategories']
        ]);
        $this->set('cprerequisite', $cprerequisite);
        $this->set('_serialize', ['cprerequisite']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cprerequisite = $this->Cprerequisites->newEntity();
        if ($this->request->is('post')) {
            $cprerequisite = $this->Cprerequisites->patchEntity($cprerequisite, $this->request->data);
            if ($this->Cprerequisites->save($cprerequisite)) {
                $this->Flash->success(__('The cprerequisite has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cprerequisite could not be saved. Please, try again.'));
            }
        }
        $ucategories = $this->Cprerequisites->Ucategories->find('list', ['limit' => 200]);
        $this->set(compact('cprerequisite', 'ucategories'));
        $this->set('_serialize', ['cprerequisite']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cprerequisite id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cprerequisite = $this->Cprerequisites->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cprerequisite = $this->Cprerequisites->patchEntity($cprerequisite, $this->request->data);
            if ($this->Cprerequisites->save($cprerequisite)) {
                $this->Flash->success(__('The cprerequisite has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cprerequisite could not be saved. Please, try again.'));
            }
        }
        $ucategories = $this->Cprerequisites->Ucategories->find('list', ['limit' => 200]);
        $this->set(compact('cprerequisite', 'ucategories'));
        $this->set('_serialize', ['cprerequisite']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cprerequisite id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cprerequisite = $this->Cprerequisites->get($id);
        if ($this->Cprerequisites->delete($cprerequisite)) {
            $this->Flash->success(__('The cprerequisite has been deleted.'));
        } else {
            $this->Flash->error(__('The cprerequisite could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
