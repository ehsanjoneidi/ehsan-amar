<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cresources Controller
 *
 * @property \App\Model\Table\CresourcesTable $Cresources
 */
class CresourcesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('cresources', $this->paginate($this->Cresources));
        $this->set('_serialize', ['cresources']);
    }

    /**
     * View method
     *
     * @param string|null $id Cresource id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cresource = $this->Cresources->get($id, [
            'contain' => ['Dunits']
        ]);
        $this->set('cresource', $cresource);
        $this->set('_serialize', ['cresource']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cresource = $this->Cresources->newEntity();
        if ($this->request->is('post')) {
            $cresource = $this->Cresources->patchEntity($cresource, $this->request->data);
            if ($this->Cresources->save($cresource)) {
                $this->Flash->success(__('The cresource has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cresource could not be saved. Please, try again.'));
            }
        }
        $dunits = $this->Cresources->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('cresource', 'dunits'));
        $this->set('_serialize', ['cresource']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cresource id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cresource = $this->Cresources->get($id, [
            'contain' => ['Dunits']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cresource = $this->Cresources->patchEntity($cresource, $this->request->data);
            if ($this->Cresources->save($cresource)) {
                $this->Flash->success(__('The cresource has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cresource could not be saved. Please, try again.'));
            }
        }
        $dunits = $this->Cresources->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('cresource', 'dunits'));
        $this->set('_serialize', ['cresource']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cresource id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cresource = $this->Cresources->get($id);
        if ($this->Cresources->delete($cresource)) {
            $this->Flash->success(__('The cresource has been deleted.'));
        } else {
            $this->Flash->error(__('The cresource could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
