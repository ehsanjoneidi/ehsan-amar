<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CresourcesDunits Controller
 *
 * @property \App\Model\Table\CresourcesDunitsTable $CresourcesDunits
 */
class CresourcesDunitsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cresources', 'Dunits']
        ];
        $this->set('cresourcesDunits', $this->paginate($this->CresourcesDunits));
        $this->set('_serialize', ['cresourcesDunits']);
    }

    /**
     * View method
     *
     * @param string|null $id Cresources Dunit id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cresourcesDunit = $this->CresourcesDunits->get($id, [
            'contain' => ['Cresources', 'Dunits']
        ]);
        $this->set('cresourcesDunit', $cresourcesDunit);
        $this->set('_serialize', ['cresourcesDunit']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cresourcesDunit = $this->CresourcesDunits->newEntity();
        if ($this->request->is('post')) {
            $cresourcesDunit = $this->CresourcesDunits->patchEntity($cresourcesDunit, $this->request->data);
            if ($this->CresourcesDunits->save($cresourcesDunit)) {
                $this->Flash->success(__('The cresources dunit has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cresources dunit could not be saved. Please, try again.'));
            }
        }
        $cresources = $this->CresourcesDunits->Cresources->find('list', ['limit' => 200]);
        $dunits = $this->CresourcesDunits->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('cresourcesDunit', 'cresources', 'dunits'));
        $this->set('_serialize', ['cresourcesDunit']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cresources Dunit id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cresourcesDunit = $this->CresourcesDunits->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cresourcesDunit = $this->CresourcesDunits->patchEntity($cresourcesDunit, $this->request->data);
            if ($this->CresourcesDunits->save($cresourcesDunit)) {
                $this->Flash->success(__('The cresources dunit has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cresources dunit could not be saved. Please, try again.'));
            }
        }
        $cresources = $this->CresourcesDunits->Cresources->find('list', ['limit' => 200]);
        $dunits = $this->CresourcesDunits->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('cresourcesDunit', 'cresources', 'dunits'));
        $this->set('_serialize', ['cresourcesDunit']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cresources Dunit id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cresourcesDunit = $this->CresourcesDunits->get($id);
        if ($this->CresourcesDunits->delete($cresourcesDunit)) {
            $this->Flash->success(__('The cresources dunit has been deleted.'));
        } else {
            $this->Flash->error(__('The cresources dunit could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
