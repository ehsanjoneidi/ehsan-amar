<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cversions Controller
 *
 * @property \App\Model\Table\CversionsTable $Cversions
 */
class CversionsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dcourses']
        ];
        $this->set('cversions', $this->paginate($this->Cversions));
        $this->set('_serialize', ['cversions']);
    }

    /**
     * View method
     *
     * @param string|null $id Cversion id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cversion = $this->Cversions->get($id, [
            'contain' => ['Dcourses']
        ]);
        $this->set('cversion', $cversion);
        $this->set('_serialize', ['cversion']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cversion = $this->Cversions->newEntity();
        if ($this->request->is('post')) {
            $cversion = $this->Cversions->patchEntity($cversion, $this->request->data);
            if ($this->Cversions->save($cversion)) {
                $this->Flash->success(__('The cversion has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cversion could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->Cversions->Dcourses->find('list', ['limit' => 200]);
        $this->set(compact('cversion', 'dcourses'));
        $this->set('_serialize', ['cversion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cversion id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cversion = $this->Cversions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cversion = $this->Cversions->patchEntity($cversion, $this->request->data);
            if ($this->Cversions->save($cversion)) {
                $this->Flash->success(__('The cversion has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cversion could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->Cversions->Dcourses->find('list', ['limit' => 200]);
        $this->set(compact('cversion', 'dcourses'));
        $this->set('_serialize', ['cversion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cversion id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cversion = $this->Cversions->get($id);
        if ($this->Cversions->delete($cversion)) {
            $this->Flash->success(__('The cversion has been deleted.'));
        } else {
            $this->Flash->error(__('The cversion could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
