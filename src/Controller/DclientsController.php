<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dclients Controller
 *
 * @property \App\Model\Table\DclientsTable $Dclients
 */
class DclientsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('dclients', $this->paginate($this->Dclients));
        $this->set('_serialize', ['dclients']);
    }

    /**
     * View method
     *
     * @param string|null $id Dclient id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dclient = $this->Dclients->get($id, [
            'contain' => ['Dcourses', 'Dunits']
        ]);
        $this->set('dclient', $dclient);
        $this->set('_serialize', ['dclient']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dclient = $this->Dclients->newEntity();
        if ($this->request->is('post')) {
            $dclient = $this->Dclients->patchEntity($dclient, $this->request->data);
            if ($this->Dclients->save($dclient)) {
                $this->Flash->success(__('The dclient has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dclient could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('dclient'));
        $this->set('_serialize', ['dclient']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dclient id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dclient = $this->Dclients->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dclient = $this->Dclients->patchEntity($dclient, $this->request->data);
            if ($this->Dclients->save($dclient)) {
                $this->Flash->success(__('The dclient has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dclient could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('dclient'));
        $this->set('_serialize', ['dclient']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dclient id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dclient = $this->Dclients->get($id);
        if ($this->Dclients->delete($dclient)) {
            $this->Flash->success(__('The dclient has been deleted.'));
        } else {
            $this->Flash->error(__('The dclient could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
