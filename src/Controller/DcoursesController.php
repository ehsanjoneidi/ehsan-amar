<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dcourses Controller
 *
 * @property \App\Model\Table\DcoursesTable $Dcourses
 */
class DcoursesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Qawards', 'Isources', 'Sareas', 'Ccategories', 'Users', 'Dclients']
        ];
        $this->set('dcourses', $this->paginate($this->Dcourses));
        $this->set('_serialize', ['dcourses']);
    }

    /**
     * View method
     *
     * @param string|null $id Dcourse id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dcourse = $this->Dcourses->get($id, [
            'contain' => ['Qawards', 'Isources', 'Sareas', 'Ccategories', 'Users', 'Dclients', 'Dunits', 'Fcareers', 'Precognitions', 'Cversions']
        ]);
        $this->set('dcourse', $dcourse);
        $this->set('_serialize', ['dcourse']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dcourse = $this->Dcourses->newEntity();
        if ($this->request->is('post')) {
            $dcourse = $this->Dcourses->patchEntity($dcourse, $this->request->data);
            if ($this->Dcourses->save($dcourse)) {
                $this->Flash->success(__('The dcourse has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dcourse could not be saved. Please, try again.'));
            }
        }
        $qawards = $this->Dcourses->Qawards->find('list', ['limit' => 200]);
        $isources = $this->Dcourses->Isources->find('list', ['limit' => 200]);
        $sareas = $this->Dcourses->Sareas->find('list', ['limit' => 200]);
        $ccategories = $this->Dcourses->Ccategories->find('list', ['limit' => 200]);
        $users = $this->Dcourses->Users->find('list', ['limit' => 200]);
        $dclients = $this->Dcourses->Dclients->find('list', ['limit' => 200]);
        $dunits = $this->Dcourses->Dunits->find('list', ['limit' => 200]);
        $fcareers = $this->Dcourses->Fcareers->find('list', ['limit' => 200]);
        $precognitions = $this->Dcourses->Precognitions->find('list', ['limit' => 200]);
        $this->set(compact('dcourse', 'qawards', 'isources', 'sareas', 'ccategories', 'users', 'dclients', 'dunits', 'fcareers', 'precognitions'));
        $this->set('_serialize', ['dcourse']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dcourse id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dcourse = $this->Dcourses->get($id, [
            'contain' => ['Dunits', 'Fcareers', 'Precognitions']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dcourse = $this->Dcourses->patchEntity($dcourse, $this->request->data);
            if ($this->Dcourses->save($dcourse)) {
                $this->Flash->success(__('The dcourse has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dcourse could not be saved. Please, try again.'));
            }
        }
        $qawards = $this->Dcourses->Qawards->find('list', ['limit' => 200]);
        $isources = $this->Dcourses->Isources->find('list', ['limit' => 200]);
        $sareas = $this->Dcourses->Sareas->find('list', ['limit' => 200]);
        $ccategories = $this->Dcourses->Ccategories->find('list', ['limit' => 200]);
        $users = $this->Dcourses->Users->find('list', ['limit' => 200]);
        $dclients = $this->Dcourses->Dclients->find('list', ['limit' => 200]);
        $dunits = $this->Dcourses->Dunits->find('list', ['limit' => 200]);
        $fcareers = $this->Dcourses->Fcareers->find('list', ['limit' => 200]);
        $precognitions = $this->Dcourses->Precognitions->find('list', ['limit' => 200]);
        $this->set(compact('dcourse', 'qawards', 'isources', 'sareas', 'ccategories', 'users', 'dclients', 'dunits', 'fcareers', 'precognitions'));
        $this->set('_serialize', ['dcourse']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dcourse id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dcourse = $this->Dcourses->get($id);
        if ($this->Dcourses->delete($dcourse)) {
            $this->Flash->success(__('The dcourse has been deleted.'));
        } else {
            $this->Flash->error(__('The dcourse could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
