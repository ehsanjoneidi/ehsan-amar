<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DcoursesDunits Controller
 *
 * @property \App\Model\Table\DcoursesDunitsTable $DcoursesDunits
 */
class DcoursesDunitsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dcourses', 'Dunits']
        ];
        $this->set('dcoursesDunits', $this->paginate($this->DcoursesDunits));
        $this->set('_serialize', ['dcoursesDunits']);
    }

    /**
     * View method
     *
     * @param string|null $id Dcourses Dunit id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dcoursesDunit = $this->DcoursesDunits->get($id, [
            'contain' => ['Dcourses', 'Dunits']
        ]);
        $this->set('dcoursesDunit', $dcoursesDunit);
        $this->set('_serialize', ['dcoursesDunit']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dcoursesDunit = $this->DcoursesDunits->newEntity();
        if ($this->request->is('post')) {
            $dcoursesDunit = $this->DcoursesDunits->patchEntity($dcoursesDunit, $this->request->data);
            if ($this->DcoursesDunits->save($dcoursesDunit)) {
                $this->Flash->success(__('The dcourses dunit has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dcourses dunit could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->DcoursesDunits->Dcourses->find('list', ['limit' => 200]);
        $dunits = $this->DcoursesDunits->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('dcoursesDunit', 'dcourses', 'dunits'));
        $this->set('_serialize', ['dcoursesDunit']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dcourses Dunit id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dcoursesDunit = $this->DcoursesDunits->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dcoursesDunit = $this->DcoursesDunits->patchEntity($dcoursesDunit, $this->request->data);
            if ($this->DcoursesDunits->save($dcoursesDunit)) {
                $this->Flash->success(__('The dcourses dunit has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dcourses dunit could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->DcoursesDunits->Dcourses->find('list', ['limit' => 200]);
        $dunits = $this->DcoursesDunits->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('dcoursesDunit', 'dcourses', 'dunits'));
        $this->set('_serialize', ['dcoursesDunit']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dcourses Dunit id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dcoursesDunit = $this->DcoursesDunits->get($id);
        if ($this->DcoursesDunits->delete($dcoursesDunit)) {
            $this->Flash->success(__('The dcourses dunit has been deleted.'));
        } else {
            $this->Flash->error(__('The dcourses dunit could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
