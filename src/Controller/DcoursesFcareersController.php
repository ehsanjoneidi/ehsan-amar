<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DcoursesFcareers Controller
 *
 * @property \App\Model\Table\DcoursesFcareersTable $DcoursesFcareers
 */
class DcoursesFcareersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dcourses', 'Fcareers']
        ];
        $this->set('dcoursesFcareers', $this->paginate($this->DcoursesFcareers));
        $this->set('_serialize', ['dcoursesFcareers']);
    }

    /**
     * View method
     *
     * @param string|null $id Dcourses Fcareer id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dcoursesFcareer = $this->DcoursesFcareers->get($id, [
            'contain' => ['Dcourses', 'Fcareers']
        ]);
        $this->set('dcoursesFcareer', $dcoursesFcareer);
        $this->set('_serialize', ['dcoursesFcareer']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dcoursesFcareer = $this->DcoursesFcareers->newEntity();
        if ($this->request->is('post')) {
            $dcoursesFcareer = $this->DcoursesFcareers->patchEntity($dcoursesFcareer, $this->request->data);
            if ($this->DcoursesFcareers->save($dcoursesFcareer)) {
                $this->Flash->success(__('The dcourses fcareer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dcourses fcareer could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->DcoursesFcareers->Dcourses->find('list', ['limit' => 200]);
        $fcareers = $this->DcoursesFcareers->Fcareers->find('list', ['limit' => 200]);
        $this->set(compact('dcoursesFcareer', 'dcourses', 'fcareers'));
        $this->set('_serialize', ['dcoursesFcareer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dcourses Fcareer id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dcoursesFcareer = $this->DcoursesFcareers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dcoursesFcareer = $this->DcoursesFcareers->patchEntity($dcoursesFcareer, $this->request->data);
            if ($this->DcoursesFcareers->save($dcoursesFcareer)) {
                $this->Flash->success(__('The dcourses fcareer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dcourses fcareer could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->DcoursesFcareers->Dcourses->find('list', ['limit' => 200]);
        $fcareers = $this->DcoursesFcareers->Fcareers->find('list', ['limit' => 200]);
        $this->set(compact('dcoursesFcareer', 'dcourses', 'fcareers'));
        $this->set('_serialize', ['dcoursesFcareer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dcourses Fcareer id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dcoursesFcareer = $this->DcoursesFcareers->get($id);
        if ($this->DcoursesFcareers->delete($dcoursesFcareer)) {
            $this->Flash->success(__('The dcourses fcareer has been deleted.'));
        } else {
            $this->Flash->error(__('The dcourses fcareer could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
