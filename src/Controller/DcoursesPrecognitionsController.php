<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DcoursesPrecognitions Controller
 *
 * @property \App\Model\Table\DcoursesPrecognitionsTable $DcoursesPrecognitions
 */
class DcoursesPrecognitionsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dcourses', 'Precognitions']
        ];
        $this->set('dcoursesPrecognitions', $this->paginate($this->DcoursesPrecognitions));
        $this->set('_serialize', ['dcoursesPrecognitions']);
    }

    /**
     * View method
     *
     * @param string|null $id Dcourses Precognition id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dcoursesPrecognition = $this->DcoursesPrecognitions->get($id, [
            'contain' => ['Dcourses', 'Precognitions']
        ]);
        $this->set('dcoursesPrecognition', $dcoursesPrecognition);
        $this->set('_serialize', ['dcoursesPrecognition']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dcoursesPrecognition = $this->DcoursesPrecognitions->newEntity();
        if ($this->request->is('post')) {
            $dcoursesPrecognition = $this->DcoursesPrecognitions->patchEntity($dcoursesPrecognition, $this->request->data);
            if ($this->DcoursesPrecognitions->save($dcoursesPrecognition)) {
                $this->Flash->success(__('The dcourses precognition has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dcourses precognition could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->DcoursesPrecognitions->Dcourses->find('list', ['limit' => 200]);
        $precognitions = $this->DcoursesPrecognitions->Precognitions->find('list', ['limit' => 200]);
        $this->set(compact('dcoursesPrecognition', 'dcourses', 'precognitions'));
        $this->set('_serialize', ['dcoursesPrecognition']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dcourses Precognition id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dcoursesPrecognition = $this->DcoursesPrecognitions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dcoursesPrecognition = $this->DcoursesPrecognitions->patchEntity($dcoursesPrecognition, $this->request->data);
            if ($this->DcoursesPrecognitions->save($dcoursesPrecognition)) {
                $this->Flash->success(__('The dcourses precognition has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dcourses precognition could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->DcoursesPrecognitions->Dcourses->find('list', ['limit' => 200]);
        $precognitions = $this->DcoursesPrecognitions->Precognitions->find('list', ['limit' => 200]);
        $this->set(compact('dcoursesPrecognition', 'dcourses', 'precognitions'));
        $this->set('_serialize', ['dcoursesPrecognition']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dcourses Precognition id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dcoursesPrecognition = $this->DcoursesPrecognitions->get($id);
        if ($this->DcoursesPrecognitions->delete($dcoursesPrecognition)) {
            $this->Flash->success(__('The dcourses precognition has been deleted.'));
        } else {
            $this->Flash->error(__('The dcourses precognition could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
