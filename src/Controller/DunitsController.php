<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dunits Controller
 *
 * @property \App\Model\Table\DunitsTable $Dunits
 */
class DunitsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Dclients', 'Oyears', 'Osemesters', 'Ucategories']
        ];
        $this->set('dunits', $this->paginate($this->Dunits));
        $this->set('_serialize', ['dunits']);
    }

    /**
     * View method
     *
     * @param string|null $id Dunit id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dunit = $this->Dunits->get($id, [
            'contain' => ['Users', 'Dclients', 'Oyears', 'Osemesters', 'Ucategories', 'Ccontents', 'Cresources', 'Dcourses', 'Loutcomes', 'Smodes', 'Tactivities']
        ]);
        $this->set('dunit', $dunit);
        $this->set('_serialize', ['dunit']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dunit = $this->Dunits->newEntity();
        if ($this->request->is('post')) {
            $dunit = $this->Dunits->patchEntity($dunit, $this->request->data);
            if ($this->Dunits->save($dunit)) {
                $this->Flash->success(__('The dunit has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dunit could not be saved. Please, try again.'));
            }
        }
        $users = $this->Dunits->Users->find('list', ['limit' => 200]);
        $dclients = $this->Dunits->Dclients->find('list', ['limit' => 200]);
        $oyears = $this->Dunits->Oyears->find('list', ['limit' => 200]);
        $osemesters = $this->Dunits->Osemesters->find('list', ['limit' => 200]);
        $ucategories = $this->Dunits->Ucategories->find('list', ['limit' => 200]);
        $ccontents = $this->Dunits->Ccontents->find('list', ['limit' => 200]);
        $cresources = $this->Dunits->Cresources->find('list', ['limit' => 200]);
        $dcourses = $this->Dunits->Dcourses->find('list', ['limit' => 200]);
        $loutcomes = $this->Dunits->Loutcomes->find('list', ['limit' => 200]);
        $smodes = $this->Dunits->Smodes->find('list', ['limit' => 200]);
        $tactivities = $this->Dunits->Tactivities->find('list', ['limit' => 200]);
        $this->set(compact('dunit', 'users', 'dclients', 'oyears', 'osemesters', 'ucategories', 'ccontents', 'cresources', 'dcourses', 'loutcomes', 'smodes', 'tactivities'));
        $this->set('_serialize', ['dunit']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dunit id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dunit = $this->Dunits->get($id, [
            'contain' => ['Ccontents', 'Cresources', 'Dcourses', 'Loutcomes', 'Smodes', 'Tactivities']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dunit = $this->Dunits->patchEntity($dunit, $this->request->data);
            if ($this->Dunits->save($dunit)) {
                $this->Flash->success(__('The dunit has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dunit could not be saved. Please, try again.'));
            }
        }
        $users = $this->Dunits->Users->find('list', ['limit' => 200]);
        $dclients = $this->Dunits->Dclients->find('list', ['limit' => 200]);
        $oyears = $this->Dunits->Oyears->find('list', ['limit' => 200]);
        $osemesters = $this->Dunits->Osemesters->find('list', ['limit' => 200]);
        $ucategories = $this->Dunits->Ucategories->find('list', ['limit' => 200]);
        $ccontents = $this->Dunits->Ccontents->find('list', ['limit' => 200]);
        $cresources = $this->Dunits->Cresources->find('list', ['limit' => 200]);
        $dcourses = $this->Dunits->Dcourses->find('list', ['limit' => 200]);
        $loutcomes = $this->Dunits->Loutcomes->find('list', ['limit' => 200]);
        $smodes = $this->Dunits->Smodes->find('list', ['limit' => 200]);
        $tactivities = $this->Dunits->Tactivities->find('list', ['limit' => 200]);
        $this->set(compact('dunit', 'users', 'dclients', 'oyears', 'osemesters', 'ucategories', 'ccontents', 'cresources', 'dcourses', 'loutcomes', 'smodes', 'tactivities'));
        $this->set('_serialize', ['dunit']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dunit id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dunit = $this->Dunits->get($id);
        if ($this->Dunits->delete($dunit)) {
            $this->Flash->success(__('The dunit has been deleted.'));
        } else {
            $this->Flash->error(__('The dunit could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
