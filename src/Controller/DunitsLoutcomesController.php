<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DunitsLoutcomes Controller
 *
 * @property \App\Model\Table\DunitsLoutcomesTable $DunitsLoutcomes
 */
class DunitsLoutcomesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dunits', 'Loutcomes']
        ];
        $this->set('dunitsLoutcomes', $this->paginate($this->DunitsLoutcomes));
        $this->set('_serialize', ['dunitsLoutcomes']);
    }

    /**
     * View method
     *
     * @param string|null $id Dunits Loutcome id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dunitsLoutcome = $this->DunitsLoutcomes->get($id, [
            'contain' => ['Dunits', 'Loutcomes']
        ]);
        $this->set('dunitsLoutcome', $dunitsLoutcome);
        $this->set('_serialize', ['dunitsLoutcome']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dunitsLoutcome = $this->DunitsLoutcomes->newEntity();
        if ($this->request->is('post')) {
            $dunitsLoutcome = $this->DunitsLoutcomes->patchEntity($dunitsLoutcome, $this->request->data);
            if ($this->DunitsLoutcomes->save($dunitsLoutcome)) {
                $this->Flash->success(__('The dunits loutcome has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dunits loutcome could not be saved. Please, try again.'));
            }
        }
        $dunits = $this->DunitsLoutcomes->Dunits->find('list', ['limit' => 200]);
        $loutcomes = $this->DunitsLoutcomes->Loutcomes->find('list', ['limit' => 200]);
        $this->set(compact('dunitsLoutcome', 'dunits', 'loutcomes'));
        $this->set('_serialize', ['dunitsLoutcome']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dunits Loutcome id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dunitsLoutcome = $this->DunitsLoutcomes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dunitsLoutcome = $this->DunitsLoutcomes->patchEntity($dunitsLoutcome, $this->request->data);
            if ($this->DunitsLoutcomes->save($dunitsLoutcome)) {
                $this->Flash->success(__('The dunits loutcome has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dunits loutcome could not be saved. Please, try again.'));
            }
        }
        $dunits = $this->DunitsLoutcomes->Dunits->find('list', ['limit' => 200]);
        $loutcomes = $this->DunitsLoutcomes->Loutcomes->find('list', ['limit' => 200]);
        $this->set(compact('dunitsLoutcome', 'dunits', 'loutcomes'));
        $this->set('_serialize', ['dunitsLoutcome']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dunits Loutcome id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dunitsLoutcome = $this->DunitsLoutcomes->get($id);
        if ($this->DunitsLoutcomes->delete($dunitsLoutcome)) {
            $this->Flash->success(__('The dunits loutcome has been deleted.'));
        } else {
            $this->Flash->error(__('The dunits loutcome could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
