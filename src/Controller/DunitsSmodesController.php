<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DunitsSmodes Controller
 *
 * @property \App\Model\Table\DunitsSmodesTable $DunitsSmodes
 */
class DunitsSmodesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dunits', 'Smodes']
        ];
        $this->set('dunitsSmodes', $this->paginate($this->DunitsSmodes));
        $this->set('_serialize', ['dunitsSmodes']);
    }

    /**
     * View method
     *
     * @param string|null $id Dunits Smode id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dunitsSmode = $this->DunitsSmodes->get($id, [
            'contain' => ['Dunits', 'Smodes']
        ]);
        $this->set('dunitsSmode', $dunitsSmode);
        $this->set('_serialize', ['dunitsSmode']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dunitsSmode = $this->DunitsSmodes->newEntity();
        if ($this->request->is('post')) {
            $dunitsSmode = $this->DunitsSmodes->patchEntity($dunitsSmode, $this->request->data);
            if ($this->DunitsSmodes->save($dunitsSmode)) {
                $this->Flash->success(__('The dunits smode has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dunits smode could not be saved. Please, try again.'));
            }
        }
        $dunits = $this->DunitsSmodes->Dunits->find('list', ['limit' => 200]);
        $smodes = $this->DunitsSmodes->Smodes->find('list', ['limit' => 200]);
        $this->set(compact('dunitsSmode', 'dunits', 'smodes'));
        $this->set('_serialize', ['dunitsSmode']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dunits Smode id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dunitsSmode = $this->DunitsSmodes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dunitsSmode = $this->DunitsSmodes->patchEntity($dunitsSmode, $this->request->data);
            if ($this->DunitsSmodes->save($dunitsSmode)) {
                $this->Flash->success(__('The dunits smode has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dunits smode could not be saved. Please, try again.'));
            }
        }
        $dunits = $this->DunitsSmodes->Dunits->find('list', ['limit' => 200]);
        $smodes = $this->DunitsSmodes->Smodes->find('list', ['limit' => 200]);
        $this->set(compact('dunitsSmode', 'dunits', 'smodes'));
        $this->set('_serialize', ['dunitsSmode']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dunits Smode id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dunitsSmode = $this->DunitsSmodes->get($id);
        if ($this->DunitsSmodes->delete($dunitsSmode)) {
            $this->Flash->success(__('The dunits smode has been deleted.'));
        } else {
            $this->Flash->error(__('The dunits smode could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
