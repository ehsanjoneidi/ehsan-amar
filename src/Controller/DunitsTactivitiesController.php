<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DunitsTactivities Controller
 *
 * @property \App\Model\Table\DunitsTactivitiesTable $DunitsTactivities
 */
class DunitsTactivitiesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tactivities', 'Dunits']
        ];
        $this->set('dunitsTactivities', $this->paginate($this->DunitsTactivities));
        $this->set('_serialize', ['dunitsTactivities']);
    }

    /**
     * View method
     *
     * @param string|null $id Dunits Tactivity id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dunitsTactivity = $this->DunitsTactivities->get($id, [
            'contain' => ['Tactivities', 'Dunits']
        ]);
        $this->set('dunitsTactivity', $dunitsTactivity);
        $this->set('_serialize', ['dunitsTactivity']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dunitsTactivity = $this->DunitsTactivities->newEntity();
        if ($this->request->is('post')) {
            $dunitsTactivity = $this->DunitsTactivities->patchEntity($dunitsTactivity, $this->request->data);
            if ($this->DunitsTactivities->save($dunitsTactivity)) {
                $this->Flash->success(__('The dunits tactivity has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dunits tactivity could not be saved. Please, try again.'));
            }
        }
        $tactivities = $this->DunitsTactivities->Tactivities->find('list', ['limit' => 200]);
        $dunits = $this->DunitsTactivities->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('dunitsTactivity', 'tactivities', 'dunits'));
        $this->set('_serialize', ['dunitsTactivity']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dunits Tactivity id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dunitsTactivity = $this->DunitsTactivities->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dunitsTactivity = $this->DunitsTactivities->patchEntity($dunitsTactivity, $this->request->data);
            if ($this->DunitsTactivities->save($dunitsTactivity)) {
                $this->Flash->success(__('The dunits tactivity has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dunits tactivity could not be saved. Please, try again.'));
            }
        }
        $tactivities = $this->DunitsTactivities->Tactivities->find('list', ['limit' => 200]);
        $dunits = $this->DunitsTactivities->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('dunitsTactivity', 'tactivities', 'dunits'));
        $this->set('_serialize', ['dunitsTactivity']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dunits Tactivity id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dunitsTactivity = $this->DunitsTactivities->get($id);
        if ($this->DunitsTactivities->delete($dunitsTactivity)) {
            $this->Flash->success(__('The dunits tactivity has been deleted.'));
        } else {
            $this->Flash->error(__('The dunits tactivity could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
