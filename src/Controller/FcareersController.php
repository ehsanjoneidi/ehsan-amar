<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fcareers Controller
 *
 * @property \App\Model\Table\FcareersTable $Fcareers
 */
class FcareersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('fcareers', $this->paginate($this->Fcareers));
        $this->set('_serialize', ['fcareers']);
    }

    /**
     * View method
     *
     * @param string|null $id Fcareer id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fcareer = $this->Fcareers->get($id, [
            'contain' => ['Dcourses']
        ]);
        $this->set('fcareer', $fcareer);
        $this->set('_serialize', ['fcareer']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fcareer = $this->Fcareers->newEntity();
        if ($this->request->is('post')) {
            $fcareer = $this->Fcareers->patchEntity($fcareer, $this->request->data);
            if ($this->Fcareers->save($fcareer)) {
                $this->Flash->success(__('The fcareer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The fcareer could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->Fcareers->Dcourses->find('list', ['limit' => 200]);
        $this->set(compact('fcareer', 'dcourses'));
        $this->set('_serialize', ['fcareer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fcareer id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fcareer = $this->Fcareers->get($id, [
            'contain' => ['Dcourses']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fcareer = $this->Fcareers->patchEntity($fcareer, $this->request->data);
            if ($this->Fcareers->save($fcareer)) {
                $this->Flash->success(__('The fcareer has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The fcareer could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->Fcareers->Dcourses->find('list', ['limit' => 200]);
        $this->set(compact('fcareer', 'dcourses'));
        $this->set('_serialize', ['fcareer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fcareer id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fcareer = $this->Fcareers->get($id);
        if ($this->Fcareers->delete($fcareer)) {
            $this->Flash->success(__('The fcareer has been deleted.'));
        } else {
            $this->Flash->error(__('The fcareer could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
