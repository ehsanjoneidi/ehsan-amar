<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Isources Controller
 *
 * @property \App\Model\Table\IsourcesTable $Isources
 */
class IsourcesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('isources', $this->paginate($this->Isources));
        $this->set('_serialize', ['isources']);
    }

    /**
     * View method
     *
     * @param string|null $id Isource id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $isource = $this->Isources->get($id, [
            'contain' => ['Dcourses']
        ]);
        $this->set('isource', $isource);
        $this->set('_serialize', ['isource']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $isource = $this->Isources->newEntity();
        if ($this->request->is('post')) {
            $isource = $this->Isources->patchEntity($isource, $this->request->data);
            if ($this->Isources->save($isource)) {
                $this->Flash->success(__('The isource has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The isource could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('isource'));
        $this->set('_serialize', ['isource']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Isource id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $isource = $this->Isources->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $isource = $this->Isources->patchEntity($isource, $this->request->data);
            if ($this->Isources->save($isource)) {
                $this->Flash->success(__('The isource has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The isource could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('isource'));
        $this->set('_serialize', ['isource']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Isource id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $isource = $this->Isources->get($id);
        if ($this->Isources->delete($isource)) {
            $this->Flash->success(__('The isource has been deleted.'));
        } else {
            $this->Flash->error(__('The isource could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
