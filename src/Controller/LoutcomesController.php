<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Loutcomes Controller
 *
 * @property \App\Model\Table\LoutcomesTable $Loutcomes
 */
class LoutcomesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('loutcomes', $this->paginate($this->Loutcomes));
        $this->set('_serialize', ['loutcomes']);
    }

    /**
     * View method
     *
     * @param string|null $id Loutcome id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $loutcome = $this->Loutcomes->get($id, [
            'contain' => ['Assstrategies', 'Dunits']
        ]);
        $this->set('loutcome', $loutcome);
        $this->set('_serialize', ['loutcome']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $loutcome = $this->Loutcomes->newEntity();
        if ($this->request->is('post')) {
            $loutcome = $this->Loutcomes->patchEntity($loutcome, $this->request->data);
            if ($this->Loutcomes->save($loutcome)) {
                $this->Flash->success(__('The loutcome has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The loutcome could not be saved. Please, try again.'));
            }
        }
        $assstrategies = $this->Loutcomes->Assstrategies->find('list', ['limit' => 200]);
        $dunits = $this->Loutcomes->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('loutcome', 'assstrategies', 'dunits'));
        $this->set('_serialize', ['loutcome']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Loutcome id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $loutcome = $this->Loutcomes->get($id, [
            'contain' => ['Assstrategies', 'Dunits']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $loutcome = $this->Loutcomes->patchEntity($loutcome, $this->request->data);
            if ($this->Loutcomes->save($loutcome)) {
                $this->Flash->success(__('The loutcome has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The loutcome could not be saved. Please, try again.'));
            }
        }
        $assstrategies = $this->Loutcomes->Assstrategies->find('list', ['limit' => 200]);
        $dunits = $this->Loutcomes->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('loutcome', 'assstrategies', 'dunits'));
        $this->set('_serialize', ['loutcome']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Loutcome id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $loutcome = $this->Loutcomes->get($id);
        if ($this->Loutcomes->delete($loutcome)) {
            $this->Flash->success(__('The loutcome has been deleted.'));
        } else {
            $this->Flash->error(__('The loutcome could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
