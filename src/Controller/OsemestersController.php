<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Osemesters Controller
 *
 * @property \App\Model\Table\OsemestersTable $Osemesters
 */
class OsemestersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('osemesters', $this->paginate($this->Osemesters));
        $this->set('_serialize', ['osemesters']);
    }

    /**
     * View method
     *
     * @param string|null $id Osemester id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $osemester = $this->Osemesters->get($id, [
            'contain' => ['Dunits']
        ]);
        $this->set('osemester', $osemester);
        $this->set('_serialize', ['osemester']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $osemester = $this->Osemesters->newEntity();
        if ($this->request->is('post')) {
            $osemester = $this->Osemesters->patchEntity($osemester, $this->request->data);
            if ($this->Osemesters->save($osemester)) {
                $this->Flash->success(__('The osemester has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The osemester could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('osemester'));
        $this->set('_serialize', ['osemester']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Osemester id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $osemester = $this->Osemesters->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $osemester = $this->Osemesters->patchEntity($osemester, $this->request->data);
            if ($this->Osemesters->save($osemester)) {
                $this->Flash->success(__('The osemester has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The osemester could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('osemester'));
        $this->set('_serialize', ['osemester']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Osemester id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $osemester = $this->Osemesters->get($id);
        if ($this->Osemesters->delete($osemester)) {
            $this->Flash->success(__('The osemester has been deleted.'));
        } else {
            $this->Flash->error(__('The osemester could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
