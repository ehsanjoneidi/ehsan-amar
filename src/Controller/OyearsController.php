<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Oyears Controller
 *
 * @property \App\Model\Table\OyearsTable $Oyears
 */
class OyearsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('oyears', $this->paginate($this->Oyears));
        $this->set('_serialize', ['oyears']);
    }

    /**
     * View method
     *
     * @param string|null $id Oyear id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $oyear = $this->Oyears->get($id, [
            'contain' => ['Dunits']
        ]);
        $this->set('oyear', $oyear);
        $this->set('_serialize', ['oyear']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $oyear = $this->Oyears->newEntity();
        if ($this->request->is('post')) {
            $oyear = $this->Oyears->patchEntity($oyear, $this->request->data);
            if ($this->Oyears->save($oyear)) {
                $this->Flash->success(__('The oyear has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The oyear could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('oyear'));
        $this->set('_serialize', ['oyear']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Oyear id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $oyear = $this->Oyears->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $oyear = $this->Oyears->patchEntity($oyear, $this->request->data);
            if ($this->Oyears->save($oyear)) {
                $this->Flash->success(__('The oyear has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The oyear could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('oyear'));
        $this->set('_serialize', ['oyear']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Oyear id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $oyear = $this->Oyears->get($id);
        if ($this->Oyears->delete($oyear)) {
            $this->Flash->success(__('The oyear has been deleted.'));
        } else {
            $this->Flash->error(__('The oyear could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
