<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Precognitions Controller
 *
 * @property \App\Model\Table\PrecognitionsTable $Precognitions
 */
class PrecognitionsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('precognitions', $this->paginate($this->Precognitions));
        $this->set('_serialize', ['precognitions']);
    }

    /**
     * View method
     *
     * @param string|null $id Precognition id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $precognition = $this->Precognitions->get($id, [
            'contain' => ['Dcourses']
        ]);
        $this->set('precognition', $precognition);
        $this->set('_serialize', ['precognition']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $precognition = $this->Precognitions->newEntity();
        if ($this->request->is('post')) {
            $precognition = $this->Precognitions->patchEntity($precognition, $this->request->data);
            if ($this->Precognitions->save($precognition)) {
                $this->Flash->success(__('The precognition has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The precognition could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->Precognitions->Dcourses->find('list', ['limit' => 200]);
        $this->set(compact('precognition', 'dcourses'));
        $this->set('_serialize', ['precognition']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Precognition id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $precognition = $this->Precognitions->get($id, [
            'contain' => ['Dcourses']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $precognition = $this->Precognitions->patchEntity($precognition, $this->request->data);
            if ($this->Precognitions->save($precognition)) {
                $this->Flash->success(__('The precognition has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The precognition could not be saved. Please, try again.'));
            }
        }
        $dcourses = $this->Precognitions->Dcourses->find('list', ['limit' => 200]);
        $this->set(compact('precognition', 'dcourses'));
        $this->set('_serialize', ['precognition']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Precognition id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $precognition = $this->Precognitions->get($id);
        if ($this->Precognitions->delete($precognition)) {
            $this->Flash->success(__('The precognition has been deleted.'));
        } else {
            $this->Flash->error(__('The precognition could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
