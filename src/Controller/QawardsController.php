<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Qawards Controller
 *
 * @property \App\Model\Table\QawardsTable $Qawards
 */
class QawardsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Qlevels']
        ];
        $this->set('qawards', $this->paginate($this->Qawards));
        $this->set('_serialize', ['qawards']);
    }

    /**
     * View method
     *
     * @param string|null $id Qaward id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $qaward = $this->Qawards->get($id, [
            'contain' => ['Qlevels', 'Dcourses']
        ]);
        $this->set('qaward', $qaward);
        $this->set('_serialize', ['qaward']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $qaward = $this->Qawards->newEntity();
        if ($this->request->is('post')) {
            $qaward = $this->Qawards->patchEntity($qaward, $this->request->data);
            if ($this->Qawards->save($qaward)) {
                $this->Flash->success(__('The qaward has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The qaward could not be saved. Please, try again.'));
            }
        }
        $qlevels = $this->Qawards->Qlevels->find('list', ['limit' => 200]);
        $this->set(compact('qaward', 'qlevels'));
        $this->set('_serialize', ['qaward']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Qaward id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $qaward = $this->Qawards->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $qaward = $this->Qawards->patchEntity($qaward, $this->request->data);
            if ($this->Qawards->save($qaward)) {
                $this->Flash->success(__('The qaward has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The qaward could not be saved. Please, try again.'));
            }
        }
        $qlevels = $this->Qawards->Qlevels->find('list', ['limit' => 200]);
        $this->set(compact('qaward', 'qlevels'));
        $this->set('_serialize', ['qaward']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Qaward id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $qaward = $this->Qawards->get($id);
        if ($this->Qawards->delete($qaward)) {
            $this->Flash->success(__('The qaward has been deleted.'));
        } else {
            $this->Flash->error(__('The qaward could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
