<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Qlevels Controller
 *
 * @property \App\Model\Table\QlevelsTable $Qlevels
 */
class QlevelsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('qlevels', $this->paginate($this->Qlevels));
        $this->set('_serialize', ['qlevels']);
    }

    /**
     * View method
     *
     * @param string|null $id Qlevel id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $qlevel = $this->Qlevels->get($id, [
            'contain' => ['Qawards']
        ]);
        $this->set('qlevel', $qlevel);
        $this->set('_serialize', ['qlevel']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $qlevel = $this->Qlevels->newEntity();
        if ($this->request->is('post')) {
            $qlevel = $this->Qlevels->patchEntity($qlevel, $this->request->data);
            if ($this->Qlevels->save($qlevel)) {
                $this->Flash->success(__('The qlevel has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The qlevel could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('qlevel'));
        $this->set('_serialize', ['qlevel']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Qlevel id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $qlevel = $this->Qlevels->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $qlevel = $this->Qlevels->patchEntity($qlevel, $this->request->data);
            if ($this->Qlevels->save($qlevel)) {
                $this->Flash->success(__('The qlevel has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The qlevel could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('qlevel'));
        $this->set('_serialize', ['qlevel']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Qlevel id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $qlevel = $this->Qlevels->get($id);
        if ($this->Qlevels->delete($qlevel)) {
            $this->Flash->success(__('The qlevel has been deleted.'));
        } else {
            $this->Flash->error(__('The qlevel could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
