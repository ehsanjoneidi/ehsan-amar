<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Rcategories Controller
 *
 * @property \App\Model\Table\RcategoriesTable $Rcategories
 */
class RcategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('rcategories', $this->paginate($this->Rcategories));
        $this->set('_serialize', ['rcategories']);
    }

    /**
     * View method
     *
     * @param string|null $id Rcategory id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rcategory = $this->Rcategories->get($id, [
            'contain' => ['Ccitations']
        ]);
        $this->set('rcategory', $rcategory);
        $this->set('_serialize', ['rcategory']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rcategory = $this->Rcategories->newEntity();
        if ($this->request->is('post')) {
            $rcategory = $this->Rcategories->patchEntity($rcategory, $this->request->data);
            if ($this->Rcategories->save($rcategory)) {
                $this->Flash->success(__('The rcategory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The rcategory could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('rcategory'));
        $this->set('_serialize', ['rcategory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rcategory id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rcategory = $this->Rcategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rcategory = $this->Rcategories->patchEntity($rcategory, $this->request->data);
            if ($this->Rcategories->save($rcategory)) {
                $this->Flash->success(__('The rcategory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The rcategory could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('rcategory'));
        $this->set('_serialize', ['rcategory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rcategory id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rcategory = $this->Rcategories->get($id);
        if ($this->Rcategories->delete($rcategory)) {
            $this->Flash->success(__('The rcategory has been deleted.'));
        } else {
            $this->Flash->error(__('The rcategory could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
