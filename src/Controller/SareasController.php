<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Sareas Controller
 *
 * @property \App\Model\Table\SareasTable $Sareas
 */
class SareasController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('sareas', $this->paginate($this->Sareas));
        $this->set('_serialize', ['sareas']);
    }

    /**
     * View method
     *
     * @param string|null $id Sarea id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sarea = $this->Sareas->get($id, [
            'contain' => ['Dcourses']
        ]);
        $this->set('sarea', $sarea);
        $this->set('_serialize', ['sarea']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sarea = $this->Sareas->newEntity();
        if ($this->request->is('post')) {
            $sarea = $this->Sareas->patchEntity($sarea, $this->request->data);
            if ($this->Sareas->save($sarea)) {
                $this->Flash->success(__('The sarea has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sarea could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('sarea'));
        $this->set('_serialize', ['sarea']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sarea id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sarea = $this->Sareas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sarea = $this->Sareas->patchEntity($sarea, $this->request->data);
            if ($this->Sareas->save($sarea)) {
                $this->Flash->success(__('The sarea has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sarea could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('sarea'));
        $this->set('_serialize', ['sarea']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sarea id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sarea = $this->Sareas->get($id);
        if ($this->Sareas->delete($sarea)) {
            $this->Flash->success(__('The sarea has been deleted.'));
        } else {
            $this->Flash->error(__('The sarea could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
