<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Smodehours Controller
 *
 * @property \App\Model\Table\SmodehoursTable $Smodehours
 */
class SmodehoursController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('smodehours', $this->paginate($this->Smodehours));
        $this->set('_serialize', ['smodehours']);
    }

    /**
     * View method
     *
     * @param string|null $id Smodehour id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $smodehour = $this->Smodehours->get($id, [
            'contain' => ['Smodes']
        ]);
        $this->set('smodehour', $smodehour);
        $this->set('_serialize', ['smodehour']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $smodehour = $this->Smodehours->newEntity();
        if ($this->request->is('post')) {
            $smodehour = $this->Smodehours->patchEntity($smodehour, $this->request->data);
            if ($this->Smodehours->save($smodehour)) {
                $this->Flash->success(__('The smodehour has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The smodehour could not be saved. Please, try again.'));
            }
        }
        $smodes = $this->Smodehours->Smodes->find('list', ['limit' => 200]);
        $this->set(compact('smodehour', 'smodes'));
        $this->set('_serialize', ['smodehour']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Smodehour id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $smodehour = $this->Smodehours->get($id, [
            'contain' => ['Smodes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $smodehour = $this->Smodehours->patchEntity($smodehour, $this->request->data);
            if ($this->Smodehours->save($smodehour)) {
                $this->Flash->success(__('The smodehour has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The smodehour could not be saved. Please, try again.'));
            }
        }
        $smodes = $this->Smodehours->Smodes->find('list', ['limit' => 200]);
        $this->set(compact('smodehour', 'smodes'));
        $this->set('_serialize', ['smodehour']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Smodehour id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $smodehour = $this->Smodehours->get($id);
        if ($this->Smodehours->delete($smodehour)) {
            $this->Flash->success(__('The smodehour has been deleted.'));
        } else {
            $this->Flash->error(__('The smodehour could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
