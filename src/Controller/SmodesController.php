<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Smodes Controller
 *
 * @property \App\Model\Table\SmodesTable $Smodes
 */
class SmodesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('smodes', $this->paginate($this->Smodes));
        $this->set('_serialize', ['smodes']);
    }

    /**
     * View method
     *
     * @param string|null $id Smode id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $smode = $this->Smodes->get($id, [
            'contain' => ['Dunits', 'Smodehours']
        ]);
        $this->set('smode', $smode);
        $this->set('_serialize', ['smode']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $smode = $this->Smodes->newEntity();
        if ($this->request->is('post')) {
            $smode = $this->Smodes->patchEntity($smode, $this->request->data);
            if ($this->Smodes->save($smode)) {
                $this->Flash->success(__('The smode has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The smode could not be saved. Please, try again.'));
            }
        }
        $dunits = $this->Smodes->Dunits->find('list', ['limit' => 200]);
        $smodehours = $this->Smodes->Smodehours->find('list', ['limit' => 200]);
        $this->set(compact('smode', 'dunits', 'smodehours'));
        $this->set('_serialize', ['smode']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Smode id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $smode = $this->Smodes->get($id, [
            'contain' => ['Dunits', 'Smodehours']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $smode = $this->Smodes->patchEntity($smode, $this->request->data);
            if ($this->Smodes->save($smode)) {
                $this->Flash->success(__('The smode has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The smode could not be saved. Please, try again.'));
            }
        }
        $dunits = $this->Smodes->Dunits->find('list', ['limit' => 200]);
        $smodehours = $this->Smodes->Smodehours->find('list', ['limit' => 200]);
        $this->set(compact('smode', 'dunits', 'smodehours'));
        $this->set('_serialize', ['smode']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Smode id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $smode = $this->Smodes->get($id);
        if ($this->Smodes->delete($smode)) {
            $this->Flash->success(__('The smode has been deleted.'));
        } else {
            $this->Flash->error(__('The smode could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
