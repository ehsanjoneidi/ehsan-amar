<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SmodesSmodehours Controller
 *
 * @property \App\Model\Table\SmodesSmodehoursTable $SmodesSmodehours
 */
class SmodesSmodehoursController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Smodes', 'Smodehours']
        ];
        $this->set('smodesSmodehours', $this->paginate($this->SmodesSmodehours));
        $this->set('_serialize', ['smodesSmodehours']);
    }

    /**
     * View method
     *
     * @param string|null $id Smodes Smodehour id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $smodesSmodehour = $this->SmodesSmodehours->get($id, [
            'contain' => ['Smodes', 'Smodehours']
        ]);
        $this->set('smodesSmodehour', $smodesSmodehour);
        $this->set('_serialize', ['smodesSmodehour']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $smodesSmodehour = $this->SmodesSmodehours->newEntity();
        if ($this->request->is('post')) {
            $smodesSmodehour = $this->SmodesSmodehours->patchEntity($smodesSmodehour, $this->request->data);
            if ($this->SmodesSmodehours->save($smodesSmodehour)) {
                $this->Flash->success(__('The smodes smodehour has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The smodes smodehour could not be saved. Please, try again.'));
            }
        }
        $smodes = $this->SmodesSmodehours->Smodes->find('list', ['limit' => 200]);
        $smodehours = $this->SmodesSmodehours->Smodehours->find('list', ['limit' => 200]);
        $this->set(compact('smodesSmodehour', 'smodes', 'smodehours'));
        $this->set('_serialize', ['smodesSmodehour']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Smodes Smodehour id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $smodesSmodehour = $this->SmodesSmodehours->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $smodesSmodehour = $this->SmodesSmodehours->patchEntity($smodesSmodehour, $this->request->data);
            if ($this->SmodesSmodehours->save($smodesSmodehour)) {
                $this->Flash->success(__('The smodes smodehour has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The smodes smodehour could not be saved. Please, try again.'));
            }
        }
        $smodes = $this->SmodesSmodehours->Smodes->find('list', ['limit' => 200]);
        $smodehours = $this->SmodesSmodehours->Smodehours->find('list', ['limit' => 200]);
        $this->set(compact('smodesSmodehour', 'smodes', 'smodehours'));
        $this->set('_serialize', ['smodesSmodehour']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Smodes Smodehour id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $smodesSmodehour = $this->SmodesSmodehours->get($id);
        if ($this->SmodesSmodehours->delete($smodesSmodehour)) {
            $this->Flash->success(__('The smodes smodehour has been deleted.'));
        } else {
            $this->Flash->error(__('The smodes smodehour could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
