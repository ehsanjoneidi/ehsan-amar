<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tactivities Controller
 *
 * @property \App\Model\Table\TactivitiesTable $Tactivities
 */
class TactivitiesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('tactivities', $this->paginate($this->Tactivities));
        $this->set('_serialize', ['tactivities']);
    }

    /**
     * View method
     *
     * @param string|null $id Tactivity id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tactivity = $this->Tactivities->get($id, [
            'contain' => ['Dunits']
        ]);
        $this->set('tactivity', $tactivity);
        $this->set('_serialize', ['tactivity']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tactivity = $this->Tactivities->newEntity();
        if ($this->request->is('post')) {
            $tactivity = $this->Tactivities->patchEntity($tactivity, $this->request->data);
            if ($this->Tactivities->save($tactivity)) {
                $this->Flash->success(__('The tactivity has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tactivity could not be saved. Please, try again.'));
            }
        }
        $dunits = $this->Tactivities->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('tactivity', 'dunits'));
        $this->set('_serialize', ['tactivity']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tactivity id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tactivity = $this->Tactivities->get($id, [
            'contain' => ['Dunits']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tactivity = $this->Tactivities->patchEntity($tactivity, $this->request->data);
            if ($this->Tactivities->save($tactivity)) {
                $this->Flash->success(__('The tactivity has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tactivity could not be saved. Please, try again.'));
            }
        }
        $dunits = $this->Tactivities->Dunits->find('list', ['limit' => 200]);
        $this->set(compact('tactivity', 'dunits'));
        $this->set('_serialize', ['tactivity']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tactivity id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tactivity = $this->Tactivities->get($id);
        if ($this->Tactivities->delete($tactivity)) {
            $this->Flash->success(__('The tactivity has been deleted.'));
        } else {
            $this->Flash->error(__('The tactivity could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
