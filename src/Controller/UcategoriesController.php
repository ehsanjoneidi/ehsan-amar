<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ucategories Controller
 *
 * @property \App\Model\Table\UcategoriesTable $Ucategories
 */
class UcategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('ucategories', $this->paginate($this->Ucategories));
        $this->set('_serialize', ['ucategories']);
    }

    /**
     * View method
     *
     * @param string|null $id Ucategory id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ucategory = $this->Ucategories->get($id, [
            'contain' => ['Dunits']
        ]);
        $this->set('ucategory', $ucategory);
        $this->set('_serialize', ['ucategory']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ucategory = $this->Ucategories->newEntity();
        if ($this->request->is('post')) {
            $ucategory = $this->Ucategories->patchEntity($ucategory, $this->request->data);
            if ($this->Ucategories->save($ucategory)) {
                $this->Flash->success(__('The ucategory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ucategory could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ucategory'));
        $this->set('_serialize', ['ucategory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ucategory id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ucategory = $this->Ucategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ucategory = $this->Ucategories->patchEntity($ucategory, $this->request->data);
            if ($this->Ucategories->save($ucategory)) {
                $this->Flash->success(__('The ucategory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ucategory could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ucategory'));
        $this->set('_serialize', ['ucategory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ucategory id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ucategory = $this->Ucategories->get($id);
        if ($this->Ucategories->delete($ucategory)) {
            $this->Flash->success(__('The ucategory has been deleted.'));
        } else {
            $this->Flash->error(__('The ucategory could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
