<?php
namespace App\Model\Table;

use App\Model\Entity\Asscategory;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Asscategories Model
 *
 * @property \Cake\ORM\Association\HasMany $Assstrategies
 */
class AsscategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('asscategories');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Assstrategies', [
            'foreignKey' => 'asscategory_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('assessment_type', 'create')
            ->notEmpty('assessment_type');

        return $validator;
    }
}
