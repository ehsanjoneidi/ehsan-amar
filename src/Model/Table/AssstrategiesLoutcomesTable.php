<?php
namespace App\Model\Table;

use App\Model\Entity\AssstrategiesLoutcome;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AssstrategiesLoutcomes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Loutcomes
 * @property \Cake\ORM\Association\BelongsTo $Assstrategies
 */
class AssstrategiesLoutcomesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('assstrategies_loutcomes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Loutcomes', [
            'foreignKey' => 'loutcome_id'
        ]);
        $this->belongsTo('Assstrategies', [
            'foreignKey' => 'assstrategy_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['loutcome_id'], 'Loutcomes'));
        $rules->add($rules->existsIn(['assstrategy_id'], 'Assstrategies'));
        return $rules;
    }
}
