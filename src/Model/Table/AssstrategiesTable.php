<?php
namespace App\Model\Table;

use App\Model\Entity\Assstrategy;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Assstrategies Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Asscategories
 * @property \Cake\ORM\Association\BelongsToMany $Loutcomes
 */
class AssstrategiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('assstrategies');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Asscategories', [
            'foreignKey' => 'asscategory_id'
        ]);
        $this->belongsToMany('Loutcomes', [
            'foreignKey' => 'assstrategy_id',
            'targetForeignKey' => 'loutcome_id',
            'joinTable' => 'assstrategies_loutcomes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('ass_name');

        $validator
            ->allowEmpty('ass_description');

        $validator
            ->add('ass_week', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('ass_week');

        $validator
            ->add('ass_weightage', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('ass_weightage');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['asscategory_id'], 'Asscategories'));
        return $rules;
    }
}
