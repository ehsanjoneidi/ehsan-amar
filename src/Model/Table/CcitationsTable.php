<?php
namespace App\Model\Table;

use App\Model\Entity\Ccitation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ccitations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Rcategories
 * @property \Cake\ORM\Association\BelongsToMany $Ccontents
 */
class CcitationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ccitations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Rcategories', [
            'foreignKey' => 'rcategory_id'
        ]);
        $this->belongsToMany('Ccontents', [
            'foreignKey' => 'ccitation_id',
            'targetForeignKey' => 'ccontent_id',
            'joinTable' => 'ccontents_ccitations'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('citation_link', 'create')
            ->notEmpty('citation_link');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['rcategory_id'], 'Rcategories'));
        return $rules;
    }
}
