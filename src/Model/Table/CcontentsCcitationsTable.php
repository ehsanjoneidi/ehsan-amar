<?php
namespace App\Model\Table;

use App\Model\Entity\CcontentsCcitation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CcontentsCcitations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Ccontents
 * @property \Cake\ORM\Association\BelongsTo $Ccitations
 */
class CcontentsCcitationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ccontents_ccitations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Ccontents', [
            'foreignKey' => 'ccontent_id'
        ]);
        $this->belongsTo('Ccitations', [
            'foreignKey' => 'ccitation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ccontent_id'], 'Ccontents'));
        $rules->add($rules->existsIn(['ccitation_id'], 'Ccitations'));
        return $rules;
    }
}
