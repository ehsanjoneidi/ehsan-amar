<?php
namespace App\Model\Table;

use App\Model\Entity\Ccontent;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ccontents Model
 *
 * @property \Cake\ORM\Association\HasMany $Topics
 * @property \Cake\ORM\Association\BelongsToMany $Ccitations
 * @property \Cake\ORM\Association\BelongsToMany $Dunits
 */
class CcontentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ccontents');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Topics', [
            'foreignKey' => 'ccontent_id'
        ]);
        $this->belongsToMany('Ccitations', [
            'foreignKey' => 'ccontent_id',
            'targetForeignKey' => 'ccitation_id',
            'joinTable' => 'ccontents_ccitations'
        ]);
        $this->belongsToMany('Dunits', [
            'foreignKey' => 'ccontent_id',
            'targetForeignKey' => 'dunit_id',
            'joinTable' => 'ccontents_dunits'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('content_description', 'create')
            ->notEmpty('content_description');

        return $validator;
    }
}
