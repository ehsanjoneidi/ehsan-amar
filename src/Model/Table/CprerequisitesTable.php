<?php
namespace App\Model\Table;

use App\Model\Entity\Cprerequisite;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cprerequisites Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Ucategories
 * @property \Cake\ORM\Association\BelongsTo $Ucategories
 */
class CprerequisitesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cprerequisites');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Ucategories', [
            'foreignKey' => 'ucategoryfor_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Ucategories', [
            'foreignKey' => 'uprerequisite_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ucategoryfor_id'], 'Ucategories'));
        $rules->add($rules->existsIn(['uprerequisite_id'], 'Ucategories'));
        return $rules;
    }
}
