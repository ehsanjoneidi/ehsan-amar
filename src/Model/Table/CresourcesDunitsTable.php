<?php
namespace App\Model\Table;

use App\Model\Entity\CresourcesDunit;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CresourcesDunits Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cresources
 * @property \Cake\ORM\Association\BelongsTo $Dunits
 */
class CresourcesDunitsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cresources_dunits');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Cresources', [
            'foreignKey' => 'cresource_id'
        ]);
        $this->belongsTo('Dunits', [
            'foreignKey' => 'dunit_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cresource_id'], 'Cresources'));
        $rules->add($rules->existsIn(['dunit_id'], 'Dunits'));
        return $rules;
    }
}
