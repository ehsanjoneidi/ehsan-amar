<?php
namespace App\Model\Table;

use App\Model\Entity\Cresource;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cresources Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Dunits
 */
class CresourcesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cresources');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Dunits', [
            'foreignKey' => 'cresource_id',
            'targetForeignKey' => 'dunit_id',
            'joinTable' => 'cresources_dunits'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('resource_name', 'create')
            ->notEmpty('resource_name');

        return $validator;
    }
}
