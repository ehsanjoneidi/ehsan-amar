<?php
namespace App\Model\Table;

use App\Model\Entity\Dclient;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dclients Model
 *
 * @property \Cake\ORM\Association\HasMany $Dcourses
 * @property \Cake\ORM\Association\HasMany $Dunits
 */
class DclientsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('dclients');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Dcourses', [
            'foreignKey' => 'dclient_id'
        ]);
        $this->hasMany('Dunits', [
            'foreignKey' => 'dclient_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('client_name', 'create')
            ->notEmpty('client_name');

        return $validator;
    }
}
