<?php
namespace App\Model\Table;

use App\Model\Entity\DcoursesPrecognition;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DcoursesPrecognitions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Dcourses
 * @property \Cake\ORM\Association\BelongsTo $Precognitions
 */
class DcoursesPrecognitionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('dcourses_precognitions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Dcourses', [
            'foreignKey' => 'dcourse_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Precognitions', [
            'foreignKey' => 'precognition_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dcourse_id'], 'Dcourses'));
        $rules->add($rules->existsIn(['precognition_id'], 'Precognitions'));
        return $rules;
    }
}
