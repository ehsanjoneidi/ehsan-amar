<?php
namespace App\Model\Table;

use App\Model\Entity\Dcourse;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dcourses Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Qawards
 * @property \Cake\ORM\Association\BelongsTo $Isources
 * @property \Cake\ORM\Association\BelongsTo $Sareas
 * @property \Cake\ORM\Association\BelongsTo $Ccategories
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Dclients
 * @property \Cake\ORM\Association\HasMany $Cversions
 * @property \Cake\ORM\Association\BelongsToMany $Dunits
 * @property \Cake\ORM\Association\BelongsToMany $Fcareers
 * @property \Cake\ORM\Association\BelongsToMany $Precognitions
 */
class DcoursesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('dcourses');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Qawards', [
            'foreignKey' => 'qaward_id'
        ]);
        $this->belongsTo('Isources', [
            'foreignKey' => 'isource_id'
        ]);
        $this->belongsTo('Sareas', [
            'foreignKey' => 'sarea_id'
        ]);
        $this->belongsTo('Ccategories', [
            'foreignKey' => 'ccategory_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Dclients', [
            'foreignKey' => 'dclient_id'
        ]);
        $this->hasMany('Cversions', [
            'foreignKey' => 'dcourse_id'
        ]);
        $this->belongsToMany('Dunits', [
            'foreignKey' => 'dcourse_id',
            'targetForeignKey' => 'dunit_id',
            'joinTable' => 'dcourses_dunits'
        ]);
        $this->belongsToMany('Fcareers', [
            'foreignKey' => 'dcourse_id',
            'targetForeignKey' => 'fcareer_id',
            'joinTable' => 'dcourses_fcareers'
        ]);
        $this->belongsToMany('Precognitions', [
            'foreignKey' => 'dcourse_id',
            'targetForeignKey' => 'precognition_id',
            'joinTable' => 'dcourses_precognitions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('cname', 'create')
            ->notEmpty('cname');

        $validator
            ->allowEmpty('coverview');

        $validator
            ->add('cduration', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('cduration');

        $validator
            ->allowEmpty('ctype');

        $validator
            ->add('ccredit', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('ccredit');

        $validator
            ->allowEmpty('cstatus');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['qaward_id'], 'Qawards'));
        $rules->add($rules->existsIn(['isource_id'], 'Isources'));
        $rules->add($rules->existsIn(['sarea_id'], 'Sareas'));
        $rules->add($rules->existsIn(['ccategory_id'], 'Ccategories'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['dclient_id'], 'Dclients'));
        return $rules;
    }
}
