<?php
namespace App\Model\Table;

use App\Model\Entity\Dunit;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dunits Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Dclients
 * @property \Cake\ORM\Association\BelongsTo $Oyears
 * @property \Cake\ORM\Association\BelongsTo $Osemesters
 * @property \Cake\ORM\Association\BelongsTo $Ucategories
 * @property \Cake\ORM\Association\BelongsToMany $Ccontents
 * @property \Cake\ORM\Association\BelongsToMany $Cresources
 * @property \Cake\ORM\Association\BelongsToMany $Dcourses
 * @property \Cake\ORM\Association\BelongsToMany $Loutcomes
 * @property \Cake\ORM\Association\BelongsToMany $Smodes
 * @property \Cake\ORM\Association\BelongsToMany $Tactivities
 */
class DunitsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('dunits');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'created_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'updated_id'
        ]);
        $this->belongsTo('Dclients', [
            'foreignKey' => 'dclient_id'
        ]);
        $this->belongsTo('Oyears', [
            'foreignKey' => 'oyear_id'
        ]);
        $this->belongsTo('Osemesters', [
            'foreignKey' => 'osemester_id'
        ]);
        $this->belongsTo('Ucategories', [
            'foreignKey' => 'ucategory_id'
        ]);
        $this->belongsToMany('Ccontents', [
            'foreignKey' => 'dunit_id',
            'targetForeignKey' => 'ccontent_id',
            'joinTable' => 'ccontents_dunits'
        ]);
        $this->belongsToMany('Cresources', [
            'foreignKey' => 'dunit_id',
            'targetForeignKey' => 'cresource_id',
            'joinTable' => 'cresources_dunits'
        ]);
        $this->belongsToMany('Dcourses', [
            'foreignKey' => 'dunit_id',
            'targetForeignKey' => 'dcourse_id',
            'joinTable' => 'dcourses_dunits'
        ]);
        $this->belongsToMany('Loutcomes', [
            'foreignKey' => 'dunit_id',
            'targetForeignKey' => 'loutcome_id',
            'joinTable' => 'dunits_loutcomes'
        ]);
        $this->belongsToMany('Smodes', [
            'foreignKey' => 'dunit_id',
            'targetForeignKey' => 'smode_id',
            'joinTable' => 'dunits_smodes'
        ]);
        $this->belongsToMany('Tactivities', [
            'foreignKey' => 'dunit_id',
            'targetForeignKey' => 'tactivity_id',
            'joinTable' => 'dunits_tactivities'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('unit_name', 'create')
            ->notEmpty('unit_name');

        $validator
            ->allowEmpty('unit_type');

        $validator
            ->allowEmpty('unit_overview');

        $validator
            ->add('unit_credits', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('unit_credits');

        $validator
            ->allowEmpty('unit_status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['created_id'], 'Users'));
        $rules->add($rules->existsIn(['updated_id'], 'Users'));
        $rules->add($rules->existsIn(['dclient_id'], 'Dclients'));
        $rules->add($rules->existsIn(['oyear_id'], 'Oyears'));
        $rules->add($rules->existsIn(['osemester_id'], 'Osemesters'));
        $rules->add($rules->existsIn(['ucategory_id'], 'Ucategories'));
        return $rules;
    }
}
