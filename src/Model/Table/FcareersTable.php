<?php
namespace App\Model\Table;

use App\Model\Entity\Fcareer;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fcareers Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Dcourses
 */
class FcareersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fcareers');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Dcourses', [
            'foreignKey' => 'fcareer_id',
            'targetForeignKey' => 'dcourse_id',
            'joinTable' => 'dcourses_fcareers'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('career_description', 'create')
            ->notEmpty('career_description');

        return $validator;
    }
}
