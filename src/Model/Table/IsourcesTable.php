<?php
namespace App\Model\Table;

use App\Model\Entity\Isource;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Isources Model
 *
 * @property \Cake\ORM\Association\HasMany $Dcourses
 */
class IsourcesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('isources');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Dcourses', [
            'foreignKey' => 'isource_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('source_name', 'create')
            ->notEmpty('source_name');

        return $validator;
    }
}
