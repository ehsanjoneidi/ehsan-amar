<?php
namespace App\Model\Table;

use App\Model\Entity\Loutcome;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Loutcomes Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Assstrategies
 * @property \Cake\ORM\Association\BelongsToMany $Dunits
 */
class LoutcomesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('loutcomes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Assstrategies', [
            'foreignKey' => 'loutcome_id',
            'targetForeignKey' => 'assstrategy_id',
            'joinTable' => 'assstrategies_loutcomes'
        ]);
        $this->belongsToMany('Dunits', [
            'foreignKey' => 'loutcome_id',
            'targetForeignKey' => 'dunit_id',
            'joinTable' => 'dunits_loutcomes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('outcome_description', 'create')
            ->notEmpty('outcome_description');

        return $validator;
    }
}
