<?php
namespace App\Model\Table;

use App\Model\Entity\Osemester;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Osemesters Model
 *
 * @property \Cake\ORM\Association\HasMany $Dunits
 */
class OsemestersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('osemesters');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Dunits', [
            'foreignKey' => 'osemester_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('semester_number', 'valid', ['rule' => 'numeric'])
            ->requirePresence('semester_number', 'create')
            ->notEmpty('semester_number');

        return $validator;
    }
}
