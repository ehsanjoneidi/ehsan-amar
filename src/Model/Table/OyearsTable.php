<?php
namespace App\Model\Table;

use App\Model\Entity\Oyear;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Oyears Model
 *
 * @property \Cake\ORM\Association\HasMany $Dunits
 */
class OyearsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('oyears');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Dunits', [
            'foreignKey' => 'oyear_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('year_number', 'valid', ['rule' => 'numeric'])
            ->requirePresence('year_number', 'create')
            ->notEmpty('year_number');

        return $validator;
    }
}
