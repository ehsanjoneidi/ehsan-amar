<?php
namespace App\Model\Table;

use App\Model\Entity\Precognition;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Precognitions Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Dcourses
 */
class PrecognitionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('precognitions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Dcourses', [
            'foreignKey' => 'precognition_id',
            'targetForeignKey' => 'dcourse_id',
            'joinTable' => 'dcourses_precognitions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('recognition_name', 'create')
            ->notEmpty('recognition_name');

        return $validator;
    }
}
