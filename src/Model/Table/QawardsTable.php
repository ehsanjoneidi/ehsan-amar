<?php
namespace App\Model\Table;

use App\Model\Entity\Qaward;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Qawards Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Qlevels
 * @property \Cake\ORM\Association\HasMany $Dcourses
 */
class QawardsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('qawards');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Qlevels', [
            'foreignKey' => 'qlevel_id'
        ]);
        $this->hasMany('Dcourses', [
            'foreignKey' => 'qaward_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('award_name', 'create')
            ->notEmpty('award_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['qlevel_id'], 'Qlevels'));
        return $rules;
    }
}
