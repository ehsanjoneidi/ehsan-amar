<?php
namespace App\Model\Table;

use App\Model\Entity\Qlevel;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Qlevels Model
 *
 * @property \Cake\ORM\Association\HasMany $Qawards
 */
class QlevelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('qlevels');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Qawards', [
            'foreignKey' => 'qlevel_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('level_number', 'valid', ['rule' => 'numeric'])
            ->requirePresence('level_number', 'create')
            ->notEmpty('level_number');

        return $validator;
    }
}
