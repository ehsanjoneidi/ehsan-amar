<?php
namespace App\Model\Table;

use App\Model\Entity\Sarea;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sareas Model
 *
 * @property \Cake\ORM\Association\HasMany $Dcourses
 */
class SareasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sareas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Dcourses', [
            'foreignKey' => 'sarea_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('area_name', 'create')
            ->notEmpty('area_name');

        return $validator;
    }
}
