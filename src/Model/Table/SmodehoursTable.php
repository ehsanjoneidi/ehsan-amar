<?php
namespace App\Model\Table;

use App\Model\Entity\Smodehour;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Smodehours Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Smodes
 */
class SmodehoursTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('smodehours');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Smodes', [
            'foreignKey' => 'smodehour_id',
            'targetForeignKey' => 'smode_id',
            'joinTable' => 'smodes_smodehours'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('numberof_hrs', 'valid', ['rule' => 'numeric'])
            ->requirePresence('numberof_hrs', 'create')
            ->notEmpty('numberof_hrs');

        return $validator;
    }
}
