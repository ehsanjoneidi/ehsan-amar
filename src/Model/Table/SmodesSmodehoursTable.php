<?php
namespace App\Model\Table;

use App\Model\Entity\SmodesSmodehour;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SmodesSmodehours Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Smodes
 * @property \Cake\ORM\Association\BelongsTo $Smodehours
 */
class SmodesSmodehoursTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('smodes_smodehours');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Smodes', [
            'foreignKey' => 'smode_id'
        ]);
        $this->belongsTo('Smodehours', [
            'foreignKey' => 'smodehour_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['smode_id'], 'Smodes'));
        $rules->add($rules->existsIn(['smodehour_id'], 'Smodehours'));
        return $rules;
    }
}
