<?php
namespace App\Model\Table;

use App\Model\Entity\Smode;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Smodes Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Dunits
 * @property \Cake\ORM\Association\BelongsToMany $Smodehours
 */
class SmodesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('smodes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Dunits', [
            'foreignKey' => 'smode_id',
            'targetForeignKey' => 'dunit_id',
            'joinTable' => 'dunits_smodes'
        ]);
        $this->belongsToMany('Smodehours', [
            'foreignKey' => 'smode_id',
            'targetForeignKey' => 'smodehour_id',
            'joinTable' => 'smodes_smodehours'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('mode_name', 'create')
            ->notEmpty('mode_name');

        return $validator;
    }
}
