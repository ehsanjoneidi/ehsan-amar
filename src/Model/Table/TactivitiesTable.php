<?php
namespace App\Model\Table;

use App\Model\Entity\Tactivity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tactivities Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Dunits
 */
class TactivitiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tactivities');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Dunits', [
            'foreignKey' => 'tactivity_id',
            'targetForeignKey' => 'dunit_id',
            'joinTable' => 'dunits_tactivities'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('activity_name', 'create')
            ->notEmpty('activity_name');

        return $validator;
    }
}
