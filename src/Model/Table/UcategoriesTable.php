<?php
namespace App\Model\Table;

use App\Model\Entity\Ucategory;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ucategories Model
 *
 * @property \Cake\ORM\Association\HasMany $Dunits
 */
class UcategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ucategories');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Dunits', [
            'foreignKey' => 'ucategory_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('ucname', 'create')
            ->notEmpty('ucname');

        return $validator;
    }
}
