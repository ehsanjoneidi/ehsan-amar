<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $asscategory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $asscategory->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Asscategories'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Assstrategies'), ['controller' => 'Assstrategies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assstrategy'), ['controller' => 'Assstrategies', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="asscategories form large-10 medium-9 columns">
    <?= $this->Form->create($asscategory) ?>
    <fieldset>
        <legend><?= __('Edit Asscategory') ?></legend>
        <?php
            echo $this->Form->input('assessment_type');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
