<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Asscategory'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Assstrategies'), ['controller' => 'Assstrategies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assstrategy'), ['controller' => 'Assstrategies', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="asscategories index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('assessment_type') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($asscategories as $asscategory): ?>
        <tr>
            <td><?= $this->Number->format($asscategory->id) ?></td>
            <td><?= h($asscategory->assessment_type) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $asscategory->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $asscategory->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $asscategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $asscategory->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
