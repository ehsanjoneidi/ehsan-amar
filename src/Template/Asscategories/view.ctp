<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Asscategory'), ['action' => 'edit', $asscategory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Asscategory'), ['action' => 'delete', $asscategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $asscategory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Asscategories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Asscategory'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Assstrategies'), ['controller' => 'Assstrategies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assstrategy'), ['controller' => 'Assstrategies', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="asscategories view large-10 medium-9 columns">
    <h2><?= h($asscategory->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Assessment Type') ?></h6>
            <p><?= h($asscategory->assessment_type) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($asscategory->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Assstrategies') ?></h4>
    <?php if (!empty($asscategory->assstrategies)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Ass Name') ?></th>
            <th><?= __('Ass Description') ?></th>
            <th><?= __('Ass Week') ?></th>
            <th><?= __('Ass Weightage') ?></th>
            <th><?= __('Asscategory Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($asscategory->assstrategies as $assstrategies): ?>
        <tr>
            <td><?= h($assstrategies->id) ?></td>
            <td><?= h($assstrategies->ass_name) ?></td>
            <td><?= h($assstrategies->ass_description) ?></td>
            <td><?= h($assstrategies->ass_week) ?></td>
            <td><?= h($assstrategies->ass_weightage) ?></td>
            <td><?= h($assstrategies->asscategory_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Assstrategies', 'action' => 'view', $assstrategies->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Assstrategies', 'action' => 'edit', $assstrategies->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Assstrategies', 'action' => 'delete', $assstrategies->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assstrategies->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
