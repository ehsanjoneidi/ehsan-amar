<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $assstrategy->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $assstrategy->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Assstrategies'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Asscategories'), ['controller' => 'Asscategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Asscategory'), ['controller' => 'Asscategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="assstrategies form large-10 medium-9 columns">
    <?= $this->Form->create($assstrategy) ?>
    <fieldset>
        <legend><?= __('Edit Assstrategy') ?></legend>
        <?php
            echo $this->Form->input('ass_name');
            echo $this->Form->input('ass_description');
            echo $this->Form->input('ass_week');
            echo $this->Form->input('ass_weightage');
            echo $this->Form->input('asscategory_id', ['options' => $asscategories, 'empty' => true]);
            echo $this->Form->input('loutcomes._ids', ['options' => $loutcomes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
