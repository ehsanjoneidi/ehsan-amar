<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Assstrategy'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Asscategories'), ['controller' => 'Asscategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Asscategory'), ['controller' => 'Asscategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="assstrategies index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('ass_name') ?></th>
            <th><?= $this->Paginator->sort('ass_description') ?></th>
            <th><?= $this->Paginator->sort('ass_week') ?></th>
            <th><?= $this->Paginator->sort('ass_weightage') ?></th>
            <th><?= $this->Paginator->sort('asscategory_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($assstrategies as $assstrategy): ?>
        <tr>
            <td><?= $this->Number->format($assstrategy->id) ?></td>
            <td><?= h($assstrategy->ass_name) ?></td>
            <td><?= h($assstrategy->ass_description) ?></td>
            <td><?= $this->Number->format($assstrategy->ass_week) ?></td>
            <td><?= $this->Number->format($assstrategy->ass_weightage) ?></td>
            <td>
                <?= $assstrategy->has('asscategory') ? $this->Html->link($assstrategy->asscategory->id, ['controller' => 'Asscategories', 'action' => 'view', $assstrategy->asscategory->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $assstrategy->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $assstrategy->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $assstrategy->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assstrategy->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
