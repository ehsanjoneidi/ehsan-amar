<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Assstrategy'), ['action' => 'edit', $assstrategy->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Assstrategy'), ['action' => 'delete', $assstrategy->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assstrategy->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Assstrategies'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assstrategy'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Asscategories'), ['controller' => 'Asscategories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Asscategory'), ['controller' => 'Asscategories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="assstrategies view large-10 medium-9 columns">
    <h2><?= h($assstrategy->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Ass Name') ?></h6>
            <p><?= h($assstrategy->ass_name) ?></p>
            <h6 class="subheader"><?= __('Ass Description') ?></h6>
            <p><?= h($assstrategy->ass_description) ?></p>
            <h6 class="subheader"><?= __('Asscategory') ?></h6>
            <p><?= $assstrategy->has('asscategory') ? $this->Html->link($assstrategy->asscategory->id, ['controller' => 'Asscategories', 'action' => 'view', $assstrategy->asscategory->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($assstrategy->id) ?></p>
            <h6 class="subheader"><?= __('Ass Week') ?></h6>
            <p><?= $this->Number->format($assstrategy->ass_week) ?></p>
            <h6 class="subheader"><?= __('Ass Weightage') ?></h6>
            <p><?= $this->Number->format($assstrategy->ass_weightage) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Loutcomes') ?></h4>
    <?php if (!empty($assstrategy->loutcomes)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Outcome Description') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($assstrategy->loutcomes as $loutcomes): ?>
        <tr>
            <td><?= h($loutcomes->id) ?></td>
            <td><?= h($loutcomes->outcome_description) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Loutcomes', 'action' => 'view', $loutcomes->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Loutcomes', 'action' => 'edit', $loutcomes->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Loutcomes', 'action' => 'delete', $loutcomes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $loutcomes->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
