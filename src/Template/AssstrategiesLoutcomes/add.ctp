<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Assstrategies Loutcomes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Assstrategies'), ['controller' => 'Assstrategies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assstrategy'), ['controller' => 'Assstrategies', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="assstrategiesLoutcomes form large-10 medium-9 columns">
    <?= $this->Form->create($assstrategiesLoutcome) ?>
    <fieldset>
        <legend><?= __('Add Assstrategies Loutcome') ?></legend>
        <?php
            echo $this->Form->input('loutcome_id', ['options' => $loutcomes, 'empty' => true]);
            echo $this->Form->input('assstrategy_id', ['options' => $assstrategies, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
