<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Assstrategies Loutcome'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Assstrategies'), ['controller' => 'Assstrategies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assstrategy'), ['controller' => 'Assstrategies', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="assstrategiesLoutcomes index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('loutcome_id') ?></th>
            <th><?= $this->Paginator->sort('assstrategy_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($assstrategiesLoutcomes as $assstrategiesLoutcome): ?>
        <tr>
            <td><?= $this->Number->format($assstrategiesLoutcome->id) ?></td>
            <td>
                <?= $assstrategiesLoutcome->has('loutcome') ? $this->Html->link($assstrategiesLoutcome->loutcome->id, ['controller' => 'Loutcomes', 'action' => 'view', $assstrategiesLoutcome->loutcome->id]) : '' ?>
            </td>
            <td>
                <?= $assstrategiesLoutcome->has('assstrategy') ? $this->Html->link($assstrategiesLoutcome->assstrategy->id, ['controller' => 'Assstrategies', 'action' => 'view', $assstrategiesLoutcome->assstrategy->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $assstrategiesLoutcome->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $assstrategiesLoutcome->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $assstrategiesLoutcome->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assstrategiesLoutcome->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
