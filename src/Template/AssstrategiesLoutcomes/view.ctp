<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Assstrategies Loutcome'), ['action' => 'edit', $assstrategiesLoutcome->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Assstrategies Loutcome'), ['action' => 'delete', $assstrategiesLoutcome->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assstrategiesLoutcome->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Assstrategies Loutcomes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assstrategies Loutcome'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Assstrategies'), ['controller' => 'Assstrategies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assstrategy'), ['controller' => 'Assstrategies', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="assstrategiesLoutcomes view large-10 medium-9 columns">
    <h2><?= h($assstrategiesLoutcome->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Loutcome') ?></h6>
            <p><?= $assstrategiesLoutcome->has('loutcome') ? $this->Html->link($assstrategiesLoutcome->loutcome->id, ['controller' => 'Loutcomes', 'action' => 'view', $assstrategiesLoutcome->loutcome->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Assstrategy') ?></h6>
            <p><?= $assstrategiesLoutcome->has('assstrategy') ? $this->Html->link($assstrategiesLoutcome->assstrategy->id, ['controller' => 'Assstrategies', 'action' => 'view', $assstrategiesLoutcome->assstrategy->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($assstrategiesLoutcome->id) ?></p>
        </div>
    </div>
</div>
