<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ccategory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ccategory->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ccategories'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="ccategories form large-10 medium-9 columns">
    <?= $this->Form->create($ccategory) ?>
    <fieldset>
        <legend><?= __('Edit Ccategory') ?></legend>
        <?php
            echo $this->Form->input('ccategory_name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
