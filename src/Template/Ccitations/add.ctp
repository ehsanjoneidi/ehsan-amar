<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Ccitations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Rcategories'), ['controller' => 'Rcategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Rcategory'), ['controller' => 'Rcategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="ccitations form large-10 medium-9 columns">
    <?= $this->Form->create($ccitation) ?>
    <fieldset>
        <legend><?= __('Add Ccitation') ?></legend>
        <?php
            echo $this->Form->input('citation_link');
            echo $this->Form->input('rcategory_id', ['options' => $rcategories, 'empty' => true]);
            echo $this->Form->input('ccontents._ids', ['options' => $ccontents]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
