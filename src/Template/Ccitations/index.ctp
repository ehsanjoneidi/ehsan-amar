<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Ccitation'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Rcategories'), ['controller' => 'Rcategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Rcategory'), ['controller' => 'Rcategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="ccitations index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('rcategory_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($ccitations as $ccitation): ?>
        <tr>
            <td><?= $this->Number->format($ccitation->id) ?></td>
            <td>
                <?= $ccitation->has('rcategory') ? $this->Html->link($ccitation->rcategory->id, ['controller' => 'Rcategories', 'action' => 'view', $ccitation->rcategory->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $ccitation->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ccitation->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ccitation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccitation->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
