<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Ccitation'), ['action' => 'edit', $ccitation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ccitation'), ['action' => 'delete', $ccitation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccitation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ccitations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccitation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Rcategories'), ['controller' => 'Rcategories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rcategory'), ['controller' => 'Rcategories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="ccitations view large-10 medium-9 columns">
    <h2><?= h($ccitation->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Rcategory') ?></h6>
            <p><?= $ccitation->has('rcategory') ? $this->Html->link($ccitation->rcategory->id, ['controller' => 'Rcategories', 'action' => 'view', $ccitation->rcategory->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($ccitation->id) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Citation Link') ?></h6>
            <?= $this->Text->autoParagraph(h($ccitation->citation_link)) ?>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Ccontents') ?></h4>
    <?php if (!empty($ccitation->ccontents)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Content Description') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($ccitation->ccontents as $ccontents): ?>
        <tr>
            <td><?= h($ccontents->id) ?></td>
            <td><?= h($ccontents->content_description) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Ccontents', 'action' => 'view', $ccontents->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Ccontents', 'action' => 'edit', $ccontents->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ccontents', 'action' => 'delete', $ccontents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccontents->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
