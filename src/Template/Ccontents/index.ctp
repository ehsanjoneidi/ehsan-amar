<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Ccontent'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Topics'), ['controller' => 'Topics', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Topic'), ['controller' => 'Topics', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccitations'), ['controller' => 'Ccitations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccitation'), ['controller' => 'Ccitations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="ccontents index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('content_description') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($ccontents as $ccontent): ?>
        <tr>
            <td><?= $this->Number->format($ccontent->id) ?></td>
            <td><?= h($ccontent->content_description) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $ccontent->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ccontent->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ccontent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccontent->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
