<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Ccontent'), ['action' => 'edit', $ccontent->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ccontent'), ['action' => 'delete', $ccontent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccontent->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ccontents'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccontent'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Topics'), ['controller' => 'Topics', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Topic'), ['controller' => 'Topics', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ccitations'), ['controller' => 'Ccitations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccitation'), ['controller' => 'Ccitations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="ccontents view large-10 medium-9 columns">
    <h2><?= h($ccontent->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Content Description') ?></h6>
            <p><?= h($ccontent->content_description) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($ccontent->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Topics') ?></h4>
    <?php if (!empty($ccontent->topics)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Tname') ?></th>
            <th><?= __('Tdescription') ?></th>
            <th><?= __('Ccontent Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($ccontent->topics as $topics): ?>
        <tr>
            <td><?= h($topics->id) ?></td>
            <td><?= h($topics->tname) ?></td>
            <td><?= h($topics->tdescription) ?></td>
            <td><?= h($topics->ccontent_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Topics', 'action' => 'view', $topics->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Topics', 'action' => 'edit', $topics->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Topics', 'action' => 'delete', $topics->id], ['confirm' => __('Are you sure you want to delete # {0}?', $topics->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Ccitations') ?></h4>
    <?php if (!empty($ccontent->ccitations)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Citation Link') ?></th>
            <th><?= __('Rcategory Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($ccontent->ccitations as $ccitations): ?>
        <tr>
            <td><?= h($ccitations->id) ?></td>
            <td><?= h($ccitations->citation_link) ?></td>
            <td><?= h($ccitations->rcategory_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Ccitations', 'action' => 'view', $ccitations->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Ccitations', 'action' => 'edit', $ccitations->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ccitations', 'action' => 'delete', $ccitations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccitations->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Dunits') ?></h4>
    <?php if (!empty($ccontent->dunits)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Unit Name') ?></th>
            <th><?= __('Unit Type') ?></th>
            <th><?= __('Unit Overview') ?></th>
            <th><?= __('Unit Credits') ?></th>
            <th><?= __('Unit Status') ?></th>
            <th><?= __('Created Id') ?></th>
            <th><?= __('Updated Id') ?></th>
            <th><?= __('Dclient Id') ?></th>
            <th><?= __('Oyear Id') ?></th>
            <th><?= __('Osemester Id') ?></th>
            <th><?= __('Ucategory Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($ccontent->dunits as $dunits): ?>
        <tr>
            <td><?= h($dunits->id) ?></td>
            <td><?= h($dunits->unit_name) ?></td>
            <td><?= h($dunits->unit_type) ?></td>
            <td><?= h($dunits->unit_overview) ?></td>
            <td><?= h($dunits->unit_credits) ?></td>
            <td><?= h($dunits->unit_status) ?></td>
            <td><?= h($dunits->created_id) ?></td>
            <td><?= h($dunits->updated_id) ?></td>
            <td><?= h($dunits->dclient_id) ?></td>
            <td><?= h($dunits->oyear_id) ?></td>
            <td><?= h($dunits->osemester_id) ?></td>
            <td><?= h($dunits->ucategory_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Dunits', 'action' => 'view', $dunits->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Dunits', 'action' => 'edit', $dunits->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dunits', 'action' => 'delete', $dunits->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunits->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
