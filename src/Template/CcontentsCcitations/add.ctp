<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Ccontents Ccitations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccitations'), ['controller' => 'Ccitations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccitation'), ['controller' => 'Ccitations', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="ccontentsCcitations form large-10 medium-9 columns">
    <?= $this->Form->create($ccontentsCcitation) ?>
    <fieldset>
        <legend><?= __('Add Ccontents Ccitation') ?></legend>
        <?php
            echo $this->Form->input('ccontent_id', ['options' => $ccontents, 'empty' => true]);
            echo $this->Form->input('ccitation_id', ['options' => $ccitations, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
