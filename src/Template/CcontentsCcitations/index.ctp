<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Ccontents Ccitation'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccitations'), ['controller' => 'Ccitations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccitation'), ['controller' => 'Ccitations', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="ccontentsCcitations index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('ccontent_id') ?></th>
            <th><?= $this->Paginator->sort('ccitation_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($ccontentsCcitations as $ccontentsCcitation): ?>
        <tr>
            <td><?= $this->Number->format($ccontentsCcitation->id) ?></td>
            <td>
                <?= $ccontentsCcitation->has('ccontent') ? $this->Html->link($ccontentsCcitation->ccontent->id, ['controller' => 'Ccontents', 'action' => 'view', $ccontentsCcitation->ccontent->id]) : '' ?>
            </td>
            <td>
                <?= $ccontentsCcitation->has('ccitation') ? $this->Html->link($ccontentsCcitation->ccitation->id, ['controller' => 'Ccitations', 'action' => 'view', $ccontentsCcitation->ccitation->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $ccontentsCcitation->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ccontentsCcitation->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ccontentsCcitation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccontentsCcitation->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
