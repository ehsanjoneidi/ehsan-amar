<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Ccontents Ccitation'), ['action' => 'edit', $ccontentsCcitation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ccontents Ccitation'), ['action' => 'delete', $ccontentsCcitation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccontentsCcitation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ccontents Ccitations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccontents Ccitation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ccitations'), ['controller' => 'Ccitations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccitation'), ['controller' => 'Ccitations', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="ccontentsCcitations view large-10 medium-9 columns">
    <h2><?= h($ccontentsCcitation->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Ccontent') ?></h6>
            <p><?= $ccontentsCcitation->has('ccontent') ? $this->Html->link($ccontentsCcitation->ccontent->id, ['controller' => 'Ccontents', 'action' => 'view', $ccontentsCcitation->ccontent->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Ccitation') ?></h6>
            <p><?= $ccontentsCcitation->has('ccitation') ? $this->Html->link($ccontentsCcitation->ccitation->id, ['controller' => 'Ccitations', 'action' => 'view', $ccontentsCcitation->ccitation->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($ccontentsCcitation->id) ?></p>
        </div>
    </div>
</div>
