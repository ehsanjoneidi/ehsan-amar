<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Ccontents Dunits'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="ccontentsDunits form large-10 medium-9 columns">
    <?= $this->Form->create($ccontentsDunit) ?>
    <fieldset>
        <legend><?= __('Add Ccontents Dunit') ?></legend>
        <?php
            echo $this->Form->input('ccontent_id', ['options' => $ccontents, 'empty' => true]);
            echo $this->Form->input('dunit_id', ['options' => $dunits, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
