<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Ccontents Dunit'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="ccontentsDunits index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('ccontent_id') ?></th>
            <th><?= $this->Paginator->sort('dunit_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($ccontentsDunits as $ccontentsDunit): ?>
        <tr>
            <td><?= $this->Number->format($ccontentsDunit->id) ?></td>
            <td>
                <?= $ccontentsDunit->has('ccontent') ? $this->Html->link($ccontentsDunit->ccontent->id, ['controller' => 'Ccontents', 'action' => 'view', $ccontentsDunit->ccontent->id]) : '' ?>
            </td>
            <td>
                <?= $ccontentsDunit->has('dunit') ? $this->Html->link($ccontentsDunit->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $ccontentsDunit->dunit->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $ccontentsDunit->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ccontentsDunit->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ccontentsDunit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccontentsDunit->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
