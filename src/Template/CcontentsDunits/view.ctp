<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Ccontents Dunit'), ['action' => 'edit', $ccontentsDunit->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ccontents Dunit'), ['action' => 'delete', $ccontentsDunit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccontentsDunit->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ccontents Dunits'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccontents Dunit'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="ccontentsDunits view large-10 medium-9 columns">
    <h2><?= h($ccontentsDunit->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Ccontent') ?></h6>
            <p><?= $ccontentsDunit->has('ccontent') ? $this->Html->link($ccontentsDunit->ccontent->id, ['controller' => 'Ccontents', 'action' => 'view', $ccontentsDunit->ccontent->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Dunit') ?></h6>
            <p><?= $ccontentsDunit->has('dunit') ? $this->Html->link($ccontentsDunit->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $ccontentsDunit->dunit->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($ccontentsDunit->id) ?></p>
        </div>
    </div>
</div>
