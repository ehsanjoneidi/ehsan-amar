<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cprerequisite->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cprerequisite->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Cprerequisites'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Ucategories'), ['controller' => 'Ucategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ucategory'), ['controller' => 'Ucategories', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="cprerequisites form large-10 medium-9 columns">
    <?= $this->Form->create($cprerequisite) ?>
    <fieldset>
        <legend><?= __('Edit Cprerequisite') ?></legend>
        <?php
            echo $this->Form->input('ucategoryfor_id');
            echo $this->Form->input('uprerequisite_id', ['options' => $ucategories]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
