<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Cprerequisite'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ucategories'), ['controller' => 'Ucategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ucategory'), ['controller' => 'Ucategories', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="cprerequisites index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('ucategoryfor_id') ?></th>
            <th><?= $this->Paginator->sort('uprerequisite_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($cprerequisites as $cprerequisite): ?>
        <tr>
            <td><?= $this->Number->format($cprerequisite->id) ?></td>
            <td><?= $this->Number->format($cprerequisite->ucategoryfor_id) ?></td>
            <td>
                <?= $cprerequisite->has('ucategory') ? $this->Html->link($cprerequisite->ucategory->id, ['controller' => 'Ucategories', 'action' => 'view', $cprerequisite->ucategory->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $cprerequisite->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cprerequisite->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cprerequisite->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cprerequisite->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
