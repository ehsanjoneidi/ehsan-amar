<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Cprerequisite'), ['action' => 'edit', $cprerequisite->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cprerequisite'), ['action' => 'delete', $cprerequisite->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cprerequisite->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cprerequisites'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cprerequisite'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ucategories'), ['controller' => 'Ucategories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ucategory'), ['controller' => 'Ucategories', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="cprerequisites view large-10 medium-9 columns">
    <h2><?= h($cprerequisite->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Ucategory') ?></h6>
            <p><?= $cprerequisite->has('ucategory') ? $this->Html->link($cprerequisite->ucategory->id, ['controller' => 'Ucategories', 'action' => 'view', $cprerequisite->ucategory->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($cprerequisite->id) ?></p>
            <h6 class="subheader"><?= __('Ucategoryfor Id') ?></h6>
            <p><?= $this->Number->format($cprerequisite->ucategoryfor_id) ?></p>
        </div>
    </div>
</div>
