<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cresource->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cresource->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Cresources'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="cresources form large-10 medium-9 columns">
    <?= $this->Form->create($cresource) ?>
    <fieldset>
        <legend><?= __('Edit Cresource') ?></legend>
        <?php
            echo $this->Form->input('resource_name');
            echo $this->Form->input('dunits._ids', ['options' => $dunits]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
