<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cresourcesDunit->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cresourcesDunit->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Cresources Dunits'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Cresources'), ['controller' => 'Cresources', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cresource'), ['controller' => 'Cresources', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="cresourcesDunits form large-10 medium-9 columns">
    <?= $this->Form->create($cresourcesDunit) ?>
    <fieldset>
        <legend><?= __('Edit Cresources Dunit') ?></legend>
        <?php
            echo $this->Form->input('cresource_id', ['options' => $cresources, 'empty' => true]);
            echo $this->Form->input('dunit_id', ['options' => $dunits, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
