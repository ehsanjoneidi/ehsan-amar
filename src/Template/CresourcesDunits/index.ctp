<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Cresources Dunit'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cresources'), ['controller' => 'Cresources', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cresource'), ['controller' => 'Cresources', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="cresourcesDunits index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('cresource_id') ?></th>
            <th><?= $this->Paginator->sort('dunit_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($cresourcesDunits as $cresourcesDunit): ?>
        <tr>
            <td><?= $this->Number->format($cresourcesDunit->id) ?></td>
            <td>
                <?= $cresourcesDunit->has('cresource') ? $this->Html->link($cresourcesDunit->cresource->id, ['controller' => 'Cresources', 'action' => 'view', $cresourcesDunit->cresource->id]) : '' ?>
            </td>
            <td>
                <?= $cresourcesDunit->has('dunit') ? $this->Html->link($cresourcesDunit->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $cresourcesDunit->dunit->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $cresourcesDunit->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cresourcesDunit->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cresourcesDunit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cresourcesDunit->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
