<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Cresources Dunit'), ['action' => 'edit', $cresourcesDunit->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cresources Dunit'), ['action' => 'delete', $cresourcesDunit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cresourcesDunit->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cresources Dunits'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cresources Dunit'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cresources'), ['controller' => 'Cresources', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cresource'), ['controller' => 'Cresources', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="cresourcesDunits view large-10 medium-9 columns">
    <h2><?= h($cresourcesDunit->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Cresource') ?></h6>
            <p><?= $cresourcesDunit->has('cresource') ? $this->Html->link($cresourcesDunit->cresource->id, ['controller' => 'Cresources', 'action' => 'view', $cresourcesDunit->cresource->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Dunit') ?></h6>
            <p><?= $cresourcesDunit->has('dunit') ? $this->Html->link($cresourcesDunit->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $cresourcesDunit->dunit->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($cresourcesDunit->id) ?></p>
        </div>
    </div>
</div>
