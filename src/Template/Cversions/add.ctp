<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Cversions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="cversions form large-10 medium-9 columns">
    <?= $this->Form->create($cversion) ?>
    <fieldset>
        <legend><?= __('Add Cversion') ?></legend>
        <?php
            echo $this->Form->input('dcourse_id', ['options' => $dcourses]);
            echo $this->Form->input('vlink');
            echo $this->Form->input('vnumber');
            echo $this->Form->input('vtype');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
