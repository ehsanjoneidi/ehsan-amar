<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Cversion'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="cversions index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('dcourse_id') ?></th>
            <th><?= $this->Paginator->sort('vlink') ?></th>
            <th><?= $this->Paginator->sort('vnumber') ?></th>
            <th><?= $this->Paginator->sort('vtype') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($cversions as $cversion): ?>
        <tr>
            <td><?= $this->Number->format($cversion->id) ?></td>
            <td>
                <?= $cversion->has('dcourse') ? $this->Html->link($cversion->dcourse->id, ['controller' => 'Dcourses', 'action' => 'view', $cversion->dcourse->id]) : '' ?>
            </td>
            <td><?= h($cversion->vlink) ?></td>
            <td><?= $this->Number->format($cversion->vnumber) ?></td>
            <td><?= h($cversion->vtype) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $cversion->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cversion->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cversion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cversion->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
