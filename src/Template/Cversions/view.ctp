<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Cversion'), ['action' => 'edit', $cversion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cversion'), ['action' => 'delete', $cversion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cversion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cversions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cversion'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="cversions view large-10 medium-9 columns">
    <h2><?= h($cversion->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Dcourse') ?></h6>
            <p><?= $cversion->has('dcourse') ? $this->Html->link($cversion->dcourse->id, ['controller' => 'Dcourses', 'action' => 'view', $cversion->dcourse->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Vlink') ?></h6>
            <p><?= h($cversion->vlink) ?></p>
            <h6 class="subheader"><?= __('Vtype') ?></h6>
            <p><?= h($cversion->vtype) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($cversion->id) ?></p>
            <h6 class="subheader"><?= __('Vnumber') ?></h6>
            <p><?= $this->Number->format($cversion->vnumber) ?></p>
        </div>
    </div>
</div>
