<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dclient->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dclient->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Dclients'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dclients form large-10 medium-9 columns">
    <?= $this->Form->create($dclient) ?>
    <fieldset>
        <legend><?= __('Edit Dclient') ?></legend>
        <?php
            echo $this->Form->input('client_name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
