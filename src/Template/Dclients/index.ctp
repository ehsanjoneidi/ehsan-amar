<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Dclient'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dclients index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('client_name') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dclients as $dclient): ?>
        <tr>
            <td><?= $this->Number->format($dclient->id) ?></td>
            <td><?= h($dclient->client_name) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $dclient->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dclient->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dclient->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dclient->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
