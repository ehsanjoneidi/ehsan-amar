<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Dclient'), ['action' => 'edit', $dclient->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dclient'), ['action' => 'delete', $dclient->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dclient->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dclients'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dclient'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="dclients view large-10 medium-9 columns">
    <h2><?= h($dclient->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Client Name') ?></h6>
            <p><?= h($dclient->client_name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($dclient->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Dcourses') ?></h4>
    <?php if (!empty($dclient->dcourses)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Cname') ?></th>
            <th><?= __('Coverview') ?></th>
            <th><?= __('Cduration') ?></th>
            <th><?= __('Ctype') ?></th>
            <th><?= __('Ccredit') ?></th>
            <th><?= __('Cstatus') ?></th>
            <th><?= __('Qaward Id') ?></th>
            <th><?= __('Isource Id') ?></th>
            <th><?= __('Sarea Id') ?></th>
            <th><?= __('Ccategory Id') ?></th>
            <th><?= __('User Id') ?></th>
            <th><?= __('Dclient Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dclient->dcourses as $dcourses): ?>
        <tr>
            <td><?= h($dcourses->id) ?></td>
            <td><?= h($dcourses->cname) ?></td>
            <td><?= h($dcourses->coverview) ?></td>
            <td><?= h($dcourses->cduration) ?></td>
            <td><?= h($dcourses->ctype) ?></td>
            <td><?= h($dcourses->ccredit) ?></td>
            <td><?= h($dcourses->cstatus) ?></td>
            <td><?= h($dcourses->qaward_id) ?></td>
            <td><?= h($dcourses->isource_id) ?></td>
            <td><?= h($dcourses->sarea_id) ?></td>
            <td><?= h($dcourses->ccategory_id) ?></td>
            <td><?= h($dcourses->user_id) ?></td>
            <td><?= h($dcourses->dclient_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Dcourses', 'action' => 'view', $dcourses->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Dcourses', 'action' => 'edit', $dcourses->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dcourses', 'action' => 'delete', $dcourses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcourses->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Dunits') ?></h4>
    <?php if (!empty($dclient->dunits)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Unit Name') ?></th>
            <th><?= __('Unit Type') ?></th>
            <th><?= __('Unit Overview') ?></th>
            <th><?= __('Unit Credits') ?></th>
            <th><?= __('Unit Status') ?></th>
            <th><?= __('Created Id') ?></th>
            <th><?= __('Updated Id') ?></th>
            <th><?= __('Dclient Id') ?></th>
            <th><?= __('Oyear Id') ?></th>
            <th><?= __('Osemester Id') ?></th>
            <th><?= __('Ucategory Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dclient->dunits as $dunits): ?>
        <tr>
            <td><?= h($dunits->id) ?></td>
            <td><?= h($dunits->unit_name) ?></td>
            <td><?= h($dunits->unit_type) ?></td>
            <td><?= h($dunits->unit_overview) ?></td>
            <td><?= h($dunits->unit_credits) ?></td>
            <td><?= h($dunits->unit_status) ?></td>
            <td><?= h($dunits->created_id) ?></td>
            <td><?= h($dunits->updated_id) ?></td>
            <td><?= h($dunits->dclient_id) ?></td>
            <td><?= h($dunits->oyear_id) ?></td>
            <td><?= h($dunits->osemester_id) ?></td>
            <td><?= h($dunits->ucategory_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Dunits', 'action' => 'view', $dunits->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Dunits', 'action' => 'edit', $dunits->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dunits', 'action' => 'delete', $dunits->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunits->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
