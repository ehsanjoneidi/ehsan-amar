<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Dcourses'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Qawards'), ['controller' => 'Qawards', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Qaward'), ['controller' => 'Qawards', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Isources'), ['controller' => 'Isources', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Isource'), ['controller' => 'Isources', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sareas'), ['controller' => 'Sareas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sarea'), ['controller' => 'Sareas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccategories'), ['controller' => 'Ccategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccategory'), ['controller' => 'Ccategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dclients'), ['controller' => 'Dclients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dclient'), ['controller' => 'Dclients', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cversions'), ['controller' => 'Cversions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cversion'), ['controller' => 'Cversions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fcareers'), ['controller' => 'Fcareers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fcareer'), ['controller' => 'Fcareers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Precognitions'), ['controller' => 'Precognitions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Precognition'), ['controller' => 'Precognitions', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dcourses form large-10 medium-9 columns">
    <?= $this->Form->create($dcourse) ?>
    <fieldset>
        <legend><?= __('Add Dcourse') ?></legend>
        <?php
            echo $this->Form->input('cname');
            echo $this->Form->input('coverview');
            echo $this->Form->input('cduration');
            echo $this->Form->input('ctype');
            echo $this->Form->input('ccredit');
            echo $this->Form->input('cstatus');
            echo $this->Form->input('qaward_id', ['options' => $qawards, 'empty' => true]);
            echo $this->Form->input('isource_id', ['options' => $isources, 'empty' => true]);
            echo $this->Form->input('sarea_id', ['options' => $sareas, 'empty' => true]);
            echo $this->Form->input('ccategory_id', ['options' => $ccategories, 'empty' => true]);
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('dclient_id', ['options' => $dclients, 'empty' => true]);
            echo $this->Form->input('dunits._ids', ['options' => $dunits]);
            echo $this->Form->input('fcareers._ids', ['options' => $fcareers]);
            echo $this->Form->input('precognitions._ids', ['options' => $precognitions]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
