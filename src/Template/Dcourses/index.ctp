<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Dcourse'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Qawards'), ['controller' => 'Qawards', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Qaward'), ['controller' => 'Qawards', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Isources'), ['controller' => 'Isources', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Isource'), ['controller' => 'Isources', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sareas'), ['controller' => 'Sareas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sarea'), ['controller' => 'Sareas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccategories'), ['controller' => 'Ccategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccategory'), ['controller' => 'Ccategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dclients'), ['controller' => 'Dclients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dclient'), ['controller' => 'Dclients', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cversions'), ['controller' => 'Cversions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cversion'), ['controller' => 'Cversions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fcareers'), ['controller' => 'Fcareers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fcareer'), ['controller' => 'Fcareers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Precognitions'), ['controller' => 'Precognitions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Precognition'), ['controller' => 'Precognitions', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dcourses index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('cname') ?></th>
            <th><?= $this->Paginator->sort('cduration') ?></th>
            <th><?= $this->Paginator->sort('ctype') ?></th>
            <th><?= $this->Paginator->sort('ccredit') ?></th>
            <th><?= $this->Paginator->sort('cstatus') ?></th>
            <th><?= $this->Paginator->sort('qaward_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dcourses as $dcourse): ?>
        <tr>
            <td><?= $this->Number->format($dcourse->id) ?></td>
            <td><?= h($dcourse->cname) ?></td>
            <td><?= $this->Number->format($dcourse->cduration) ?></td>
            <td><?= h($dcourse->ctype) ?></td>
            <td><?= $this->Number->format($dcourse->ccredit) ?></td>
            <td><?= h($dcourse->cstatus) ?></td>
            <td>
                <?= $dcourse->has('qaward') ? $this->Html->link($dcourse->qaward->id, ['controller' => 'Qawards', 'action' => 'view', $dcourse->qaward->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $dcourse->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dcourse->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dcourse->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcourse->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
