<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Dcourse'), ['action' => 'edit', $dcourse->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dcourse'), ['action' => 'delete', $dcourse->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcourse->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourse'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Qawards'), ['controller' => 'Qawards', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Qaward'), ['controller' => 'Qawards', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Isources'), ['controller' => 'Isources', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Isource'), ['controller' => 'Isources', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sareas'), ['controller' => 'Sareas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sarea'), ['controller' => 'Sareas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ccategories'), ['controller' => 'Ccategories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccategory'), ['controller' => 'Ccategories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dclients'), ['controller' => 'Dclients', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dclient'), ['controller' => 'Dclients', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cversions'), ['controller' => 'Cversions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cversion'), ['controller' => 'Cversions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Fcareers'), ['controller' => 'Fcareers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fcareer'), ['controller' => 'Fcareers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Precognitions'), ['controller' => 'Precognitions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Precognition'), ['controller' => 'Precognitions', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="dcourses view large-10 medium-9 columns">
    <h2><?= h($dcourse->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Cname') ?></h6>
            <p><?= h($dcourse->cname) ?></p>
            <h6 class="subheader"><?= __('Ctype') ?></h6>
            <p><?= h($dcourse->ctype) ?></p>
            <h6 class="subheader"><?= __('Cstatus') ?></h6>
            <p><?= h($dcourse->cstatus) ?></p>
            <h6 class="subheader"><?= __('Qaward') ?></h6>
            <p><?= $dcourse->has('qaward') ? $this->Html->link($dcourse->qaward->id, ['controller' => 'Qawards', 'action' => 'view', $dcourse->qaward->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Isource') ?></h6>
            <p><?= $dcourse->has('isource') ? $this->Html->link($dcourse->isource->id, ['controller' => 'Isources', 'action' => 'view', $dcourse->isource->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Sarea') ?></h6>
            <p><?= $dcourse->has('sarea') ? $this->Html->link($dcourse->sarea->id, ['controller' => 'Sareas', 'action' => 'view', $dcourse->sarea->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Ccategory') ?></h6>
            <p><?= $dcourse->has('ccategory') ? $this->Html->link($dcourse->ccategory->id, ['controller' => 'Ccategories', 'action' => 'view', $dcourse->ccategory->id]) : '' ?></p>
            <h6 class="subheader"><?= __('User') ?></h6>
            <p><?= $dcourse->has('user') ? $this->Html->link($dcourse->user->id, ['controller' => 'Users', 'action' => 'view', $dcourse->user->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Dclient') ?></h6>
            <p><?= $dcourse->has('dclient') ? $this->Html->link($dcourse->dclient->id, ['controller' => 'Dclients', 'action' => 'view', $dcourse->dclient->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($dcourse->id) ?></p>
            <h6 class="subheader"><?= __('Cduration') ?></h6>
            <p><?= $this->Number->format($dcourse->cduration) ?></p>
            <h6 class="subheader"><?= __('Ccredit') ?></h6>
            <p><?= $this->Number->format($dcourse->ccredit) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Coverview') ?></h6>
            <?= $this->Text->autoParagraph(h($dcourse->coverview)) ?>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Cversions') ?></h4>
    <?php if (!empty($dcourse->cversions)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Dcourse Id') ?></th>
            <th><?= __('Vlink') ?></th>
            <th><?= __('Vnumber') ?></th>
            <th><?= __('Vtype') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dcourse->cversions as $cversions): ?>
        <tr>
            <td><?= h($cversions->id) ?></td>
            <td><?= h($cversions->dcourse_id) ?></td>
            <td><?= h($cversions->vlink) ?></td>
            <td><?= h($cversions->vnumber) ?></td>
            <td><?= h($cversions->vtype) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Cversions', 'action' => 'view', $cversions->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Cversions', 'action' => 'edit', $cversions->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Cversions', 'action' => 'delete', $cversions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cversions->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Dunits') ?></h4>
    <?php if (!empty($dcourse->dunits)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Unit Name') ?></th>
            <th><?= __('Unit Type') ?></th>
            <th><?= __('Unit Overview') ?></th>
            <th><?= __('Unit Credits') ?></th>
            <th><?= __('Unit Status') ?></th>
            <th><?= __('Created Id') ?></th>
            <th><?= __('Updated Id') ?></th>
            <th><?= __('Dclient Id') ?></th>
            <th><?= __('Oyear Id') ?></th>
            <th><?= __('Osemester Id') ?></th>
            <th><?= __('Ucategory Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dcourse->dunits as $dunits): ?>
        <tr>
            <td><?= h($dunits->id) ?></td>
            <td><?= h($dunits->unit_name) ?></td>
            <td><?= h($dunits->unit_type) ?></td>
            <td><?= h($dunits->unit_overview) ?></td>
            <td><?= h($dunits->unit_credits) ?></td>
            <td><?= h($dunits->unit_status) ?></td>
            <td><?= h($dunits->created_id) ?></td>
            <td><?= h($dunits->updated_id) ?></td>
            <td><?= h($dunits->dclient_id) ?></td>
            <td><?= h($dunits->oyear_id) ?></td>
            <td><?= h($dunits->osemester_id) ?></td>
            <td><?= h($dunits->ucategory_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Dunits', 'action' => 'view', $dunits->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Dunits', 'action' => 'edit', $dunits->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dunits', 'action' => 'delete', $dunits->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunits->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Fcareers') ?></h4>
    <?php if (!empty($dcourse->fcareers)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Career Description') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dcourse->fcareers as $fcareers): ?>
        <tr>
            <td><?= h($fcareers->id) ?></td>
            <td><?= h($fcareers->career_description) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Fcareers', 'action' => 'view', $fcareers->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Fcareers', 'action' => 'edit', $fcareers->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Fcareers', 'action' => 'delete', $fcareers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fcareers->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Precognitions') ?></h4>
    <?php if (!empty($dcourse->precognitions)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Recognition Name') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dcourse->precognitions as $precognitions): ?>
        <tr>
            <td><?= h($precognitions->id) ?></td>
            <td><?= h($precognitions->recognition_name) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Precognitions', 'action' => 'view', $precognitions->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Precognitions', 'action' => 'edit', $precognitions->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Precognitions', 'action' => 'delete', $precognitions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $precognitions->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
