<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Dcourses Dunit'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dcoursesDunits index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('dcourse_id') ?></th>
            <th><?= $this->Paginator->sort('dunit_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dcoursesDunits as $dcoursesDunit): ?>
        <tr>
            <td><?= $this->Number->format($dcoursesDunit->id) ?></td>
            <td>
                <?= $dcoursesDunit->has('dcourse') ? $this->Html->link($dcoursesDunit->dcourse->id, ['controller' => 'Dcourses', 'action' => 'view', $dcoursesDunit->dcourse->id]) : '' ?>
            </td>
            <td>
                <?= $dcoursesDunit->has('dunit') ? $this->Html->link($dcoursesDunit->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $dcoursesDunit->dunit->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $dcoursesDunit->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dcoursesDunit->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dcoursesDunit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcoursesDunit->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
