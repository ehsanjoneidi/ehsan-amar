<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Dcourses Dunit'), ['action' => 'edit', $dcoursesDunit->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dcourses Dunit'), ['action' => 'delete', $dcoursesDunit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcoursesDunit->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses Dunits'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourses Dunit'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="dcoursesDunits view large-10 medium-9 columns">
    <h2><?= h($dcoursesDunit->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Dcourse') ?></h6>
            <p><?= $dcoursesDunit->has('dcourse') ? $this->Html->link($dcoursesDunit->dcourse->id, ['controller' => 'Dcourses', 'action' => 'view', $dcoursesDunit->dcourse->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Dunit') ?></h6>
            <p><?= $dcoursesDunit->has('dunit') ? $this->Html->link($dcoursesDunit->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $dcoursesDunit->dunit->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($dcoursesDunit->id) ?></p>
        </div>
    </div>
</div>
