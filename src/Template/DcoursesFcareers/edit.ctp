<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dcoursesFcareer->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dcoursesFcareer->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Dcourses Fcareers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fcareers'), ['controller' => 'Fcareers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fcareer'), ['controller' => 'Fcareers', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dcoursesFcareers form large-10 medium-9 columns">
    <?= $this->Form->create($dcoursesFcareer) ?>
    <fieldset>
        <legend><?= __('Edit Dcourses Fcareer') ?></legend>
        <?php
            echo $this->Form->input('dcourse_id', ['options' => $dcourses]);
            echo $this->Form->input('fcareer_id', ['options' => $fcareers]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
