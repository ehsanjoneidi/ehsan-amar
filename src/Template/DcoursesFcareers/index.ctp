<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Dcourses Fcareer'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fcareers'), ['controller' => 'Fcareers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fcareer'), ['controller' => 'Fcareers', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dcoursesFcareers index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('dcourse_id') ?></th>
            <th><?= $this->Paginator->sort('fcareer_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dcoursesFcareers as $dcoursesFcareer): ?>
        <tr>
            <td><?= $this->Number->format($dcoursesFcareer->id) ?></td>
            <td>
                <?= $dcoursesFcareer->has('dcourse') ? $this->Html->link($dcoursesFcareer->dcourse->id, ['controller' => 'Dcourses', 'action' => 'view', $dcoursesFcareer->dcourse->id]) : '' ?>
            </td>
            <td>
                <?= $dcoursesFcareer->has('fcareer') ? $this->Html->link($dcoursesFcareer->fcareer->id, ['controller' => 'Fcareers', 'action' => 'view', $dcoursesFcareer->fcareer->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $dcoursesFcareer->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dcoursesFcareer->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dcoursesFcareer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcoursesFcareer->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
