<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Dcourses Fcareer'), ['action' => 'edit', $dcoursesFcareer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dcourses Fcareer'), ['action' => 'delete', $dcoursesFcareer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcoursesFcareer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses Fcareers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourses Fcareer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Fcareers'), ['controller' => 'Fcareers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fcareer'), ['controller' => 'Fcareers', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="dcoursesFcareers view large-10 medium-9 columns">
    <h2><?= h($dcoursesFcareer->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Dcourse') ?></h6>
            <p><?= $dcoursesFcareer->has('dcourse') ? $this->Html->link($dcoursesFcareer->dcourse->id, ['controller' => 'Dcourses', 'action' => 'view', $dcoursesFcareer->dcourse->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Fcareer') ?></h6>
            <p><?= $dcoursesFcareer->has('fcareer') ? $this->Html->link($dcoursesFcareer->fcareer->id, ['controller' => 'Fcareers', 'action' => 'view', $dcoursesFcareer->fcareer->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($dcoursesFcareer->id) ?></p>
        </div>
    </div>
</div>
