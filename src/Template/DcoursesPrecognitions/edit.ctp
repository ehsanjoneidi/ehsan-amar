<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dcoursesPrecognition->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dcoursesPrecognition->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Dcourses Precognitions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Precognitions'), ['controller' => 'Precognitions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Precognition'), ['controller' => 'Precognitions', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dcoursesPrecognitions form large-10 medium-9 columns">
    <?= $this->Form->create($dcoursesPrecognition) ?>
    <fieldset>
        <legend><?= __('Edit Dcourses Precognition') ?></legend>
        <?php
            echo $this->Form->input('dcourse_id', ['options' => $dcourses]);
            echo $this->Form->input('precognition_id', ['options' => $precognitions]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
