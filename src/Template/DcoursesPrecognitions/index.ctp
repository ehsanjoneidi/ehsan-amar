<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Dcourses Precognition'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Precognitions'), ['controller' => 'Precognitions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Precognition'), ['controller' => 'Precognitions', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dcoursesPrecognitions index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('dcourse_id') ?></th>
            <th><?= $this->Paginator->sort('precognition_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dcoursesPrecognitions as $dcoursesPrecognition): ?>
        <tr>
            <td><?= $this->Number->format($dcoursesPrecognition->id) ?></td>
            <td>
                <?= $dcoursesPrecognition->has('dcourse') ? $this->Html->link($dcoursesPrecognition->dcourse->id, ['controller' => 'Dcourses', 'action' => 'view', $dcoursesPrecognition->dcourse->id]) : '' ?>
            </td>
            <td>
                <?= $dcoursesPrecognition->has('precognition') ? $this->Html->link($dcoursesPrecognition->precognition->id, ['controller' => 'Precognitions', 'action' => 'view', $dcoursesPrecognition->precognition->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $dcoursesPrecognition->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dcoursesPrecognition->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dcoursesPrecognition->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcoursesPrecognition->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
