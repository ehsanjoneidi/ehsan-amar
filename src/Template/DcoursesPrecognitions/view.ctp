<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Dcourses Precognition'), ['action' => 'edit', $dcoursesPrecognition->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dcourses Precognition'), ['action' => 'delete', $dcoursesPrecognition->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcoursesPrecognition->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses Precognitions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourses Precognition'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Precognitions'), ['controller' => 'Precognitions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Precognition'), ['controller' => 'Precognitions', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="dcoursesPrecognitions view large-10 medium-9 columns">
    <h2><?= h($dcoursesPrecognition->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Dcourse') ?></h6>
            <p><?= $dcoursesPrecognition->has('dcourse') ? $this->Html->link($dcoursesPrecognition->dcourse->id, ['controller' => 'Dcourses', 'action' => 'view', $dcoursesPrecognition->dcourse->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Precognition') ?></h6>
            <p><?= $dcoursesPrecognition->has('precognition') ? $this->Html->link($dcoursesPrecognition->precognition->id, ['controller' => 'Precognitions', 'action' => 'view', $dcoursesPrecognition->precognition->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($dcoursesPrecognition->id) ?></p>
        </div>
    </div>
</div>
