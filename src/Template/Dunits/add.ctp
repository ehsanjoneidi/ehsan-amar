<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Dunits'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dclients'), ['controller' => 'Dclients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dclient'), ['controller' => 'Dclients', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Oyears'), ['controller' => 'Oyears', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Oyear'), ['controller' => 'Oyears', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Osemesters'), ['controller' => 'Osemesters', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Osemester'), ['controller' => 'Osemesters', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ucategories'), ['controller' => 'Ucategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ucategory'), ['controller' => 'Ucategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cresources'), ['controller' => 'Cresources', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cresource'), ['controller' => 'Cresources', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tactivities'), ['controller' => 'Tactivities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tactivity'), ['controller' => 'Tactivities', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dunits form large-10 medium-9 columns">
    <?= $this->Form->create($dunit) ?>
    <fieldset>
        <legend><?= __('Add Dunit') ?></legend>
        <?php
            echo $this->Form->input('unit_name');
            echo $this->Form->input('unit_type');
            echo $this->Form->input('unit_overview');
            echo $this->Form->input('unit_credits');
            echo $this->Form->input('unit_status');
            echo $this->Form->input('created_id');
            echo $this->Form->input('updated_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('dclient_id', ['options' => $dclients, 'empty' => true]);
            echo $this->Form->input('oyear_id', ['options' => $oyears, 'empty' => true]);
            echo $this->Form->input('osemester_id', ['options' => $osemesters, 'empty' => true]);
            echo $this->Form->input('ucategory_id', ['options' => $ucategories, 'empty' => true]);
            echo $this->Form->input('ccontents._ids', ['options' => $ccontents]);
            echo $this->Form->input('cresources._ids', ['options' => $cresources]);
            echo $this->Form->input('dcourses._ids', ['options' => $dcourses]);
            echo $this->Form->input('loutcomes._ids', ['options' => $loutcomes]);
            echo $this->Form->input('smodes._ids', ['options' => $smodes]);
            echo $this->Form->input('tactivities._ids', ['options' => $tactivities]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
