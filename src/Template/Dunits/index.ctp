<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Dunit'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dclients'), ['controller' => 'Dclients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dclient'), ['controller' => 'Dclients', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Oyears'), ['controller' => 'Oyears', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Oyear'), ['controller' => 'Oyears', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Osemesters'), ['controller' => 'Osemesters', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Osemester'), ['controller' => 'Osemesters', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ucategories'), ['controller' => 'Ucategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ucategory'), ['controller' => 'Ucategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cresources'), ['controller' => 'Cresources', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cresource'), ['controller' => 'Cresources', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tactivities'), ['controller' => 'Tactivities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tactivity'), ['controller' => 'Tactivities', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dunits index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('unit_name') ?></th>
            <th><?= $this->Paginator->sort('unit_type') ?></th>
            <th><?= $this->Paginator->sort('unit_credits') ?></th>
            <th><?= $this->Paginator->sort('unit_status') ?></th>
            <th><?= $this->Paginator->sort('created_id') ?></th>
            <th><?= $this->Paginator->sort('updated_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dunits as $dunit): ?>
        <tr>
            <td><?= $this->Number->format($dunit->id) ?></td>
            <td><?= h($dunit->unit_name) ?></td>
            <td><?= h($dunit->unit_type) ?></td>
            <td><?= $this->Number->format($dunit->unit_credits) ?></td>
            <td><?= h($dunit->unit_status) ?></td>
            <td><?= $this->Number->format($dunit->created_id) ?></td>
            <td>
                <?= $dunit->has('user') ? $this->Html->link($dunit->user->id, ['controller' => 'Users', 'action' => 'view', $dunit->user->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $dunit->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dunit->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dunit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunit->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
