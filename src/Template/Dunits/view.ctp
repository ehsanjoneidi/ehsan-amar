<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Dunit'), ['action' => 'edit', $dunit->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dunit'), ['action' => 'delete', $dunit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunit->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dclients'), ['controller' => 'Dclients', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dclient'), ['controller' => 'Dclients', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Oyears'), ['controller' => 'Oyears', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Oyear'), ['controller' => 'Oyears', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Osemesters'), ['controller' => 'Osemesters', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Osemester'), ['controller' => 'Osemesters', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ucategories'), ['controller' => 'Ucategories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ucategory'), ['controller' => 'Ucategories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cresources'), ['controller' => 'Cresources', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cresource'), ['controller' => 'Cresources', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tactivities'), ['controller' => 'Tactivities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tactivity'), ['controller' => 'Tactivities', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="dunits view large-10 medium-9 columns">
    <h2><?= h($dunit->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Unit Name') ?></h6>
            <p><?= h($dunit->unit_name) ?></p>
            <h6 class="subheader"><?= __('Unit Type') ?></h6>
            <p><?= h($dunit->unit_type) ?></p>
            <h6 class="subheader"><?= __('Unit Status') ?></h6>
            <p><?= h($dunit->unit_status) ?></p>
            <h6 class="subheader"><?= __('User') ?></h6>
            <p><?= $dunit->has('user') ? $this->Html->link($dunit->user->id, ['controller' => 'Users', 'action' => 'view', $dunit->user->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Dclient') ?></h6>
            <p><?= $dunit->has('dclient') ? $this->Html->link($dunit->dclient->id, ['controller' => 'Dclients', 'action' => 'view', $dunit->dclient->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Oyear') ?></h6>
            <p><?= $dunit->has('oyear') ? $this->Html->link($dunit->oyear->id, ['controller' => 'Oyears', 'action' => 'view', $dunit->oyear->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Osemester') ?></h6>
            <p><?= $dunit->has('osemester') ? $this->Html->link($dunit->osemester->id, ['controller' => 'Osemesters', 'action' => 'view', $dunit->osemester->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Ucategory') ?></h6>
            <p><?= $dunit->has('ucategory') ? $this->Html->link($dunit->ucategory->id, ['controller' => 'Ucategories', 'action' => 'view', $dunit->ucategory->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($dunit->id) ?></p>
            <h6 class="subheader"><?= __('Unit Credits') ?></h6>
            <p><?= $this->Number->format($dunit->unit_credits) ?></p>
            <h6 class="subheader"><?= __('Created Id') ?></h6>
            <p><?= $this->Number->format($dunit->created_id) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Unit Overview') ?></h6>
            <?= $this->Text->autoParagraph(h($dunit->unit_overview)) ?>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Ccontents') ?></h4>
    <?php if (!empty($dunit->ccontents)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Content Description') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dunit->ccontents as $ccontents): ?>
        <tr>
            <td><?= h($ccontents->id) ?></td>
            <td><?= h($ccontents->content_description) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Ccontents', 'action' => 'view', $ccontents->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Ccontents', 'action' => 'edit', $ccontents->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ccontents', 'action' => 'delete', $ccontents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccontents->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Cresources') ?></h4>
    <?php if (!empty($dunit->cresources)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Resource Name') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dunit->cresources as $cresources): ?>
        <tr>
            <td><?= h($cresources->id) ?></td>
            <td><?= h($cresources->resource_name) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Cresources', 'action' => 'view', $cresources->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Cresources', 'action' => 'edit', $cresources->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Cresources', 'action' => 'delete', $cresources->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cresources->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Dcourses') ?></h4>
    <?php if (!empty($dunit->dcourses)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Cname') ?></th>
            <th><?= __('Coverview') ?></th>
            <th><?= __('Cduration') ?></th>
            <th><?= __('Ctype') ?></th>
            <th><?= __('Ccredit') ?></th>
            <th><?= __('Cstatus') ?></th>
            <th><?= __('Qaward Id') ?></th>
            <th><?= __('Isource Id') ?></th>
            <th><?= __('Sarea Id') ?></th>
            <th><?= __('Ccategory Id') ?></th>
            <th><?= __('User Id') ?></th>
            <th><?= __('Dclient Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dunit->dcourses as $dcourses): ?>
        <tr>
            <td><?= h($dcourses->id) ?></td>
            <td><?= h($dcourses->cname) ?></td>
            <td><?= h($dcourses->coverview) ?></td>
            <td><?= h($dcourses->cduration) ?></td>
            <td><?= h($dcourses->ctype) ?></td>
            <td><?= h($dcourses->ccredit) ?></td>
            <td><?= h($dcourses->cstatus) ?></td>
            <td><?= h($dcourses->qaward_id) ?></td>
            <td><?= h($dcourses->isource_id) ?></td>
            <td><?= h($dcourses->sarea_id) ?></td>
            <td><?= h($dcourses->ccategory_id) ?></td>
            <td><?= h($dcourses->user_id) ?></td>
            <td><?= h($dcourses->dclient_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Dcourses', 'action' => 'view', $dcourses->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Dcourses', 'action' => 'edit', $dcourses->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dcourses', 'action' => 'delete', $dcourses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcourses->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Loutcomes') ?></h4>
    <?php if (!empty($dunit->loutcomes)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Outcome Description') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dunit->loutcomes as $loutcomes): ?>
        <tr>
            <td><?= h($loutcomes->id) ?></td>
            <td><?= h($loutcomes->outcome_description) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Loutcomes', 'action' => 'view', $loutcomes->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Loutcomes', 'action' => 'edit', $loutcomes->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Loutcomes', 'action' => 'delete', $loutcomes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $loutcomes->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Smodes') ?></h4>
    <?php if (!empty($dunit->smodes)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Mode Name') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dunit->smodes as $smodes): ?>
        <tr>
            <td><?= h($smodes->id) ?></td>
            <td><?= h($smodes->mode_name) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Smodes', 'action' => 'view', $smodes->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Smodes', 'action' => 'edit', $smodes->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Smodes', 'action' => 'delete', $smodes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smodes->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Tactivities') ?></h4>
    <?php if (!empty($dunit->tactivities)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Activity Name') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($dunit->tactivities as $tactivities): ?>
        <tr>
            <td><?= h($tactivities->id) ?></td>
            <td><?= h($tactivities->activity_name) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Tactivities', 'action' => 'view', $tactivities->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Tactivities', 'action' => 'edit', $tactivities->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tactivities', 'action' => 'delete', $tactivities->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tactivities->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
