<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Dunits Loutcomes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dunitsLoutcomes form large-10 medium-9 columns">
    <?= $this->Form->create($dunitsLoutcome) ?>
    <fieldset>
        <legend><?= __('Add Dunits Loutcome') ?></legend>
        <?php
            echo $this->Form->input('dunit_id', ['options' => $dunits, 'empty' => true]);
            echo $this->Form->input('loutcome_id', ['options' => $loutcomes, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
