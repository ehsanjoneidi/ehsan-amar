<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Dunits Loutcome'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dunitsLoutcomes index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('dunit_id') ?></th>
            <th><?= $this->Paginator->sort('loutcome_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dunitsLoutcomes as $dunitsLoutcome): ?>
        <tr>
            <td><?= $this->Number->format($dunitsLoutcome->id) ?></td>
            <td>
                <?= $dunitsLoutcome->has('dunit') ? $this->Html->link($dunitsLoutcome->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $dunitsLoutcome->dunit->id]) : '' ?>
            </td>
            <td>
                <?= $dunitsLoutcome->has('loutcome') ? $this->Html->link($dunitsLoutcome->loutcome->id, ['controller' => 'Loutcomes', 'action' => 'view', $dunitsLoutcome->loutcome->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $dunitsLoutcome->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dunitsLoutcome->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dunitsLoutcome->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunitsLoutcome->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
