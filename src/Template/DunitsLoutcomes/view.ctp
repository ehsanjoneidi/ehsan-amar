<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Dunits Loutcome'), ['action' => 'edit', $dunitsLoutcome->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dunits Loutcome'), ['action' => 'delete', $dunitsLoutcome->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunitsLoutcome->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dunits Loutcomes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunits Loutcome'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['controller' => 'Loutcomes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Loutcome'), ['controller' => 'Loutcomes', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="dunitsLoutcomes view large-10 medium-9 columns">
    <h2><?= h($dunitsLoutcome->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Dunit') ?></h6>
            <p><?= $dunitsLoutcome->has('dunit') ? $this->Html->link($dunitsLoutcome->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $dunitsLoutcome->dunit->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Loutcome') ?></h6>
            <p><?= $dunitsLoutcome->has('loutcome') ? $this->Html->link($dunitsLoutcome->loutcome->id, ['controller' => 'Loutcomes', 'action' => 'view', $dunitsLoutcome->loutcome->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($dunitsLoutcome->id) ?></p>
        </div>
    </div>
</div>
