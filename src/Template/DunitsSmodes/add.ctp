<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Dunits Smodes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dunitsSmodes form large-10 medium-9 columns">
    <?= $this->Form->create($dunitsSmode) ?>
    <fieldset>
        <legend><?= __('Add Dunits Smode') ?></legend>
        <?php
            echo $this->Form->input('dunit_id', ['options' => $dunits, 'empty' => true]);
            echo $this->Form->input('smode_id', ['options' => $smodes, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
