<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Dunits Smode'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dunitsSmodes index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('dunit_id') ?></th>
            <th><?= $this->Paginator->sort('smode_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dunitsSmodes as $dunitsSmode): ?>
        <tr>
            <td><?= $this->Number->format($dunitsSmode->id) ?></td>
            <td>
                <?= $dunitsSmode->has('dunit') ? $this->Html->link($dunitsSmode->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $dunitsSmode->dunit->id]) : '' ?>
            </td>
            <td>
                <?= $dunitsSmode->has('smode') ? $this->Html->link($dunitsSmode->smode->id, ['controller' => 'Smodes', 'action' => 'view', $dunitsSmode->smode->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $dunitsSmode->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dunitsSmode->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dunitsSmode->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunitsSmode->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
