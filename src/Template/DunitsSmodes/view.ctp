<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Dunits Smode'), ['action' => 'edit', $dunitsSmode->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dunits Smode'), ['action' => 'delete', $dunitsSmode->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunitsSmode->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dunits Smodes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunits Smode'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="dunitsSmodes view large-10 medium-9 columns">
    <h2><?= h($dunitsSmode->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Dunit') ?></h6>
            <p><?= $dunitsSmode->has('dunit') ? $this->Html->link($dunitsSmode->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $dunitsSmode->dunit->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Smode') ?></h6>
            <p><?= $dunitsSmode->has('smode') ? $this->Html->link($dunitsSmode->smode->id, ['controller' => 'Smodes', 'action' => 'view', $dunitsSmode->smode->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($dunitsSmode->id) ?></p>
        </div>
    </div>
</div>
