<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Dunits Tactivities'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tactivities'), ['controller' => 'Tactivities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tactivity'), ['controller' => 'Tactivities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dunitsTactivities form large-10 medium-9 columns">
    <?= $this->Form->create($dunitsTactivity) ?>
    <fieldset>
        <legend><?= __('Add Dunits Tactivity') ?></legend>
        <?php
            echo $this->Form->input('tactivity_id', ['options' => $tactivities, 'empty' => true]);
            echo $this->Form->input('dunit_id', ['options' => $dunits, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
