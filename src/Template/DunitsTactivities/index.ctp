<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Dunits Tactivity'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tactivities'), ['controller' => 'Tactivities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tactivity'), ['controller' => 'Tactivities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="dunitsTactivities index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('tactivity_id') ?></th>
            <th><?= $this->Paginator->sort('dunit_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($dunitsTactivities as $dunitsTactivity): ?>
        <tr>
            <td><?= $this->Number->format($dunitsTactivity->id) ?></td>
            <td>
                <?= $dunitsTactivity->has('tactivity') ? $this->Html->link($dunitsTactivity->tactivity->id, ['controller' => 'Tactivities', 'action' => 'view', $dunitsTactivity->tactivity->id]) : '' ?>
            </td>
            <td>
                <?= $dunitsTactivity->has('dunit') ? $this->Html->link($dunitsTactivity->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $dunitsTactivity->dunit->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $dunitsTactivity->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dunitsTactivity->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dunitsTactivity->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunitsTactivity->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
