<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Dunits Tactivity'), ['action' => 'edit', $dunitsTactivity->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dunits Tactivity'), ['action' => 'delete', $dunitsTactivity->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunitsTactivity->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dunits Tactivities'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunits Tactivity'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tactivities'), ['controller' => 'Tactivities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tactivity'), ['controller' => 'Tactivities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="dunitsTactivities view large-10 medium-9 columns">
    <h2><?= h($dunitsTactivity->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Tactivity') ?></h6>
            <p><?= $dunitsTactivity->has('tactivity') ? $this->Html->link($dunitsTactivity->tactivity->id, ['controller' => 'Tactivities', 'action' => 'view', $dunitsTactivity->tactivity->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Dunit') ?></h6>
            <p><?= $dunitsTactivity->has('dunit') ? $this->Html->link($dunitsTactivity->dunit->id, ['controller' => 'Dunits', 'action' => 'view', $dunitsTactivity->dunit->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($dunitsTactivity->id) ?></p>
        </div>
    </div>
</div>
