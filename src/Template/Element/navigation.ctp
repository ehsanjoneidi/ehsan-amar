
<nav class="navbar navbar-inverse">
    <div class="container-fluid" style="margin-bottom: 5px;margin-top: 5px">
        <div class="navbar-header">
            <?=
            $this->Html->link('D A R L O', array('controller' => 'Pages', 'action' => 'home')
                    , array('class' => 'navbar-brand')
                    , array('escape' => FALSE));
            ?>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Page 1-1</a></li>
                        <li><a href="#">Page 1-2</a></li>
                        <li><a href="#">Page 1-3</a></li>
                    </ul>
                </li>
                <li><a href="#">Page 2</a></li>
                <li><a href="#">Page 3</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><?php
                    if (!($this->request->session()->read('Auth.User'))) {
                        echo '<a href="#" data-toggle ="modal" data-target ="#loginModal"><span class="glyphicon glyphicon-log-in"></span> Login </a>';
                    } else {
                        echo $this->Html->link('<span class="glyphicon glyphicon-log-out"></span> Logout'
                                , array('controller' => 'users', 'action' => 'logout')
                                , array('escape' => FALSE));
                    }
                    ?>
                </li>
            </ul>
            <!-- Modal -->
            <div class="modal fade" id="loginModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" style="padding:15px 50px;">

                            <h4><span class="glyphicon glyphicon-lock"></span> Login
                                <button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                        </div>
                        <div class="modal-body" style="padding:15px 50px;">

                            <!--  <form role="form"> -->
                            <?=
                            $this->Form->create(null, [
                                'url' => ['controller' => 'users', 'action' => 'login']
                            ])
                            ?>
                            <div class="form-group">                                  
                                <label for="usrname"><span class="glyphicon glyphicon-user"></span> Username   </label>
                                <?=
                                $this->Form->text('username', ['class' => 'form-control', 'placeholder' => 'Enter Username'])
                                ?>
                            </div>
                            <div class="form-group"> 
                                <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password </label>
                                <?=
                                $this->Form->password('password', ['class' => 'form-control', 'placeholder' => 'Enter Password'])
                                ?> 
                            </div>
                            <div class="form-group"> 
                                <?=
                                $this->Form->button('<span class="glyphicon glyphicon-off"></span> Login'
                                        , array('class' => 'btn btn-success btn-block')
                                        , array('escape' => FALSE))
                                ?>                             
                                <?= $this->Form->end() ?>  
                                <!--  </form> -->
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger btn-default pull-right" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span> Cancel</button>
                               <!-- <p>Not a member? <a href="#">Sign Up</a></p>
                                <p>Forgot <a href="#">Password?</a></p> -->
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
</nav>