<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Fcareers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="fcareers form large-10 medium-9 columns">
    <?= $this->Form->create($fcareer) ?>
    <fieldset>
        <legend><?= __('Add Fcareer') ?></legend>
        <?php
            echo $this->Form->input('career_description');
            echo $this->Form->input('dcourses._ids', ['options' => $dcourses]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
