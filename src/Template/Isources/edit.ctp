<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $isource->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $isource->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Isources'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="isources form large-10 medium-9 columns">
    <?= $this->Form->create($isource) ?>
    <fieldset>
        <legend><?= __('Edit Isource') ?></legend>
        <?php
            echo $this->Form->input('source_name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
