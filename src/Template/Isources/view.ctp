<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Isource'), ['action' => 'edit', $isource->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Isource'), ['action' => 'delete', $isource->id], ['confirm' => __('Are you sure you want to delete # {0}?', $isource->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Isources'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Isource'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="isources view large-10 medium-9 columns">
    <h2><?= h($isource->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Source Name') ?></h6>
            <p><?= h($isource->source_name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($isource->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Dcourses') ?></h4>
    <?php if (!empty($isource->dcourses)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Cname') ?></th>
            <th><?= __('Coverview') ?></th>
            <th><?= __('Cduration') ?></th>
            <th><?= __('Ctype') ?></th>
            <th><?= __('Ccredit') ?></th>
            <th><?= __('Cstatus') ?></th>
            <th><?= __('Qaward Id') ?></th>
            <th><?= __('Isource Id') ?></th>
            <th><?= __('Sarea Id') ?></th>
            <th><?= __('Ccategory Id') ?></th>
            <th><?= __('User Id') ?></th>
            <th><?= __('Dclient Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($isource->dcourses as $dcourses): ?>
        <tr>
            <td><?= h($dcourses->id) ?></td>
            <td><?= h($dcourses->cname) ?></td>
            <td><?= h($dcourses->coverview) ?></td>
            <td><?= h($dcourses->cduration) ?></td>
            <td><?= h($dcourses->ctype) ?></td>
            <td><?= h($dcourses->ccredit) ?></td>
            <td><?= h($dcourses->cstatus) ?></td>
            <td><?= h($dcourses->qaward_id) ?></td>
            <td><?= h($dcourses->isource_id) ?></td>
            <td><?= h($dcourses->sarea_id) ?></td>
            <td><?= h($dcourses->ccategory_id) ?></td>
            <td><?= h($dcourses->user_id) ?></td>
            <td><?= h($dcourses->dclient_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Dcourses', 'action' => 'view', $dcourses->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Dcourses', 'action' => 'edit', $dcourses->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dcourses', 'action' => 'delete', $dcourses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcourses->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
