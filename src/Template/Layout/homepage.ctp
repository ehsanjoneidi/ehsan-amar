<?php
/*
 * Author : Amey Kalamkar
 * Date: 8 Sep 2015 
 * Title: Home Page layout 
 * Description: Layout is specifically created for home page
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?= $this->Html->meta('icon') ?>
        <?= $this->Html->css('bootstrap.min') ?>             
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <header>

        </header>
        <div id="container ">    

            <div id="content">

                <div class="row">
                    <?= $this->fetch('content') ?>
                </div>   
                <div class="container">
                    <?php {
                        $flashMessage = $this->Flash->render();
                        if ($flashMessage !== NULL) {
                            echo '<div class="alert alert-info"><a href="#" class="close" data-dismiss="alert" style="bottom-margin:20px;">&times;</a>' . $flashMessage . '</div>';
                        }
                    }
                    ?>
                </div>
            </div>
            <footer>
            </footer>
        </div> <!-- End Of Container -->
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <?php echo $this->Html->script('bootstrap.min'); ?>
    </body>
</html>

