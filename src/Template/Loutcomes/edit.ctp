<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $loutcome->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $loutcome->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Assstrategies'), ['controller' => 'Assstrategies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assstrategy'), ['controller' => 'Assstrategies', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="loutcomes form large-10 medium-9 columns">
    <?= $this->Form->create($loutcome) ?>
    <fieldset>
        <legend><?= __('Edit Loutcome') ?></legend>
        <?php
            echo $this->Form->input('outcome_description');
            echo $this->Form->input('assstrategies._ids', ['options' => $assstrategies]);
            echo $this->Form->input('dunits._ids', ['options' => $dunits]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
