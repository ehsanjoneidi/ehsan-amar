<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Loutcome'), ['action' => 'edit', $loutcome->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Loutcome'), ['action' => 'delete', $loutcome->id], ['confirm' => __('Are you sure you want to delete # {0}?', $loutcome->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Loutcomes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Loutcome'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Assstrategies'), ['controller' => 'Assstrategies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assstrategy'), ['controller' => 'Assstrategies', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="loutcomes view large-10 medium-9 columns">
    <h2><?= h($loutcome->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Outcome Description') ?></h6>
            <p><?= h($loutcome->outcome_description) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($loutcome->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Assstrategies') ?></h4>
    <?php if (!empty($loutcome->assstrategies)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Ass Name') ?></th>
            <th><?= __('Ass Description') ?></th>
            <th><?= __('Ass Week') ?></th>
            <th><?= __('Ass Weightage') ?></th>
            <th><?= __('Asscategory Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($loutcome->assstrategies as $assstrategies): ?>
        <tr>
            <td><?= h($assstrategies->id) ?></td>
            <td><?= h($assstrategies->ass_name) ?></td>
            <td><?= h($assstrategies->ass_description) ?></td>
            <td><?= h($assstrategies->ass_week) ?></td>
            <td><?= h($assstrategies->ass_weightage) ?></td>
            <td><?= h($assstrategies->asscategory_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Assstrategies', 'action' => 'view', $assstrategies->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Assstrategies', 'action' => 'edit', $assstrategies->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Assstrategies', 'action' => 'delete', $assstrategies->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assstrategies->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Dunits') ?></h4>
    <?php if (!empty($loutcome->dunits)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Unit Name') ?></th>
            <th><?= __('Unit Type') ?></th>
            <th><?= __('Unit Overview') ?></th>
            <th><?= __('Unit Credits') ?></th>
            <th><?= __('Unit Status') ?></th>
            <th><?= __('Created Id') ?></th>
            <th><?= __('Updated Id') ?></th>
            <th><?= __('Dclient Id') ?></th>
            <th><?= __('Oyear Id') ?></th>
            <th><?= __('Osemester Id') ?></th>
            <th><?= __('Ucategory Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($loutcome->dunits as $dunits): ?>
        <tr>
            <td><?= h($dunits->id) ?></td>
            <td><?= h($dunits->unit_name) ?></td>
            <td><?= h($dunits->unit_type) ?></td>
            <td><?= h($dunits->unit_overview) ?></td>
            <td><?= h($dunits->unit_credits) ?></td>
            <td><?= h($dunits->unit_status) ?></td>
            <td><?= h($dunits->created_id) ?></td>
            <td><?= h($dunits->updated_id) ?></td>
            <td><?= h($dunits->dclient_id) ?></td>
            <td><?= h($dunits->oyear_id) ?></td>
            <td><?= h($dunits->osemester_id) ?></td>
            <td><?= h($dunits->ucategory_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Dunits', 'action' => 'view', $dunits->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Dunits', 'action' => 'edit', $dunits->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dunits', 'action' => 'delete', $dunits->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunits->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
