<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Osemester'), ['action' => 'edit', $osemester->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Osemester'), ['action' => 'delete', $osemester->id], ['confirm' => __('Are you sure you want to delete # {0}?', $osemester->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Osemesters'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Osemester'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="osemesters view large-10 medium-9 columns">
    <h2><?= h($osemester->id) ?></h2>
    <div class="row">
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($osemester->id) ?></p>
            <h6 class="subheader"><?= __('Semester Number') ?></h6>
            <p><?= $this->Number->format($osemester->semester_number) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Dunits') ?></h4>
    <?php if (!empty($osemester->dunits)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Unit Name') ?></th>
            <th><?= __('Unit Type') ?></th>
            <th><?= __('Unit Overview') ?></th>
            <th><?= __('Unit Credits') ?></th>
            <th><?= __('Unit Status') ?></th>
            <th><?= __('Created Id') ?></th>
            <th><?= __('Updated Id') ?></th>
            <th><?= __('Dclient Id') ?></th>
            <th><?= __('Oyear Id') ?></th>
            <th><?= __('Osemester Id') ?></th>
            <th><?= __('Ucategory Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($osemester->dunits as $dunits): ?>
        <tr>
            <td><?= h($dunits->id) ?></td>
            <td><?= h($dunits->unit_name) ?></td>
            <td><?= h($dunits->unit_type) ?></td>
            <td><?= h($dunits->unit_overview) ?></td>
            <td><?= h($dunits->unit_credits) ?></td>
            <td><?= h($dunits->unit_status) ?></td>
            <td><?= h($dunits->created_id) ?></td>
            <td><?= h($dunits->updated_id) ?></td>
            <td><?= h($dunits->dclient_id) ?></td>
            <td><?= h($dunits->oyear_id) ?></td>
            <td><?= h($dunits->osemester_id) ?></td>
            <td><?= h($dunits->ucategory_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Dunits', 'action' => 'view', $dunits->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Dunits', 'action' => 'edit', $dunits->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dunits', 'action' => 'delete', $dunits->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dunits->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
