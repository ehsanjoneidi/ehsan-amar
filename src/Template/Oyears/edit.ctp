<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $oyear->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $oyear->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Oyears'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="oyears form large-10 medium-9 columns">
    <?= $this->Form->create($oyear) ?>
    <fieldset>
        <legend><?= __('Edit Oyear') ?></legend>
        <?php
            echo $this->Form->input('year_number');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
