<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Qawards'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Qlevels'), ['controller' => 'Qlevels', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Qlevel'), ['controller' => 'Qlevels', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="qawards form large-10 medium-9 columns">
    <?= $this->Form->create($qaward) ?>
    <fieldset>
        <legend><?= __('Add Qaward') ?></legend>
        <?php
            echo $this->Form->input('award_name');
            echo $this->Form->input('qlevel_id', ['options' => $qlevels, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
