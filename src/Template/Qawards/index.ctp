<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Qaward'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Qlevels'), ['controller' => 'Qlevels', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Qlevel'), ['controller' => 'Qlevels', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="qawards index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('award_name') ?></th>
            <th><?= $this->Paginator->sort('qlevel_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($qawards as $qaward): ?>
        <tr>
            <td><?= $this->Number->format($qaward->id) ?></td>
            <td><?= h($qaward->award_name) ?></td>
            <td>
                <?= $qaward->has('qlevel') ? $this->Html->link($qaward->qlevel->id, ['controller' => 'Qlevels', 'action' => 'view', $qaward->qlevel->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $qaward->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $qaward->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $qaward->id], ['confirm' => __('Are you sure you want to delete # {0}?', $qaward->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
