<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $qlevel->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $qlevel->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Qlevels'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Qawards'), ['controller' => 'Qawards', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Qaward'), ['controller' => 'Qawards', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="qlevels form large-10 medium-9 columns">
    <?= $this->Form->create($qlevel) ?>
    <fieldset>
        <legend><?= __('Edit Qlevel') ?></legend>
        <?php
            echo $this->Form->input('level_number');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
