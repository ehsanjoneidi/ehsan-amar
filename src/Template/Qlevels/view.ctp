<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Qlevel'), ['action' => 'edit', $qlevel->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Qlevel'), ['action' => 'delete', $qlevel->id], ['confirm' => __('Are you sure you want to delete # {0}?', $qlevel->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Qlevels'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Qlevel'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Qawards'), ['controller' => 'Qawards', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Qaward'), ['controller' => 'Qawards', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="qlevels view large-10 medium-9 columns">
    <h2><?= h($qlevel->id) ?></h2>
    <div class="row">
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($qlevel->id) ?></p>
            <h6 class="subheader"><?= __('Level Number') ?></h6>
            <p><?= $this->Number->format($qlevel->level_number) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Qawards') ?></h4>
    <?php if (!empty($qlevel->qawards)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Award Name') ?></th>
            <th><?= __('Qlevel Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($qlevel->qawards as $qawards): ?>
        <tr>
            <td><?= h($qawards->id) ?></td>
            <td><?= h($qawards->award_name) ?></td>
            <td><?= h($qawards->qlevel_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Qawards', 'action' => 'view', $qawards->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Qawards', 'action' => 'edit', $qawards->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Qawards', 'action' => 'delete', $qawards->id], ['confirm' => __('Are you sure you want to delete # {0}?', $qawards->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
