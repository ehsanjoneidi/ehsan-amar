<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $rcategory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $rcategory->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Rcategories'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Ccitations'), ['controller' => 'Ccitations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccitation'), ['controller' => 'Ccitations', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="rcategories form large-10 medium-9 columns">
    <?= $this->Form->create($rcategory) ?>
    <fieldset>
        <legend><?= __('Edit Rcategory') ?></legend>
        <?php
            echo $this->Form->input('rcategory_description');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
