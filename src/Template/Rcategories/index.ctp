<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Rcategory'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ccitations'), ['controller' => 'Ccitations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccitation'), ['controller' => 'Ccitations', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="rcategories index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('rcategory_description') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($rcategories as $rcategory): ?>
        <tr>
            <td><?= $this->Number->format($rcategory->id) ?></td>
            <td><?= h($rcategory->rcategory_description) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $rcategory->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rcategory->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $rcategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rcategory->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
