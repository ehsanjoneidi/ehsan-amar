<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Rcategory'), ['action' => 'edit', $rcategory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Rcategory'), ['action' => 'delete', $rcategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rcategory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Rcategories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rcategory'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ccitations'), ['controller' => 'Ccitations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ccitation'), ['controller' => 'Ccitations', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="rcategories view large-10 medium-9 columns">
    <h2><?= h($rcategory->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Rcategory Description') ?></h6>
            <p><?= h($rcategory->rcategory_description) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($rcategory->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Ccitations') ?></h4>
    <?php if (!empty($rcategory->ccitations)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Citation Link') ?></th>
            <th><?= __('Rcategory Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($rcategory->ccitations as $ccitations): ?>
        <tr>
            <td><?= h($ccitations->id) ?></td>
            <td><?= h($ccitations->citation_link) ?></td>
            <td><?= h($ccitations->rcategory_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Ccitations', 'action' => 'view', $ccitations->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Ccitations', 'action' => 'edit', $ccitations->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ccitations', 'action' => 'delete', $ccitations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ccitations->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
