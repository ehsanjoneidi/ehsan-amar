<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sarea->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $sarea->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sareas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="sareas form large-10 medium-9 columns">
    <?= $this->Form->create($sarea) ?>
    <fieldset>
        <legend><?= __('Edit Sarea') ?></legend>
        <?php
            echo $this->Form->input('area_name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
