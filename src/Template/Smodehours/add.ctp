<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Smodehours'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="smodehours form large-10 medium-9 columns">
    <?= $this->Form->create($smodehour) ?>
    <fieldset>
        <legend><?= __('Add Smodehour') ?></legend>
        <?php
            echo $this->Form->input('numberof_hrs');
            echo $this->Form->input('smodes._ids', ['options' => $smodes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
