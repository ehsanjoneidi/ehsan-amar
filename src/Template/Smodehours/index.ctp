<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Smodehour'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="smodehours index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('numberof_hrs') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($smodehours as $smodehour): ?>
        <tr>
            <td><?= $this->Number->format($smodehour->id) ?></td>
            <td><?= $this->Number->format($smodehour->numberof_hrs) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $smodehour->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $smodehour->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $smodehour->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smodehour->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
