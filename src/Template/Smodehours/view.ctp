<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Smodehour'), ['action' => 'edit', $smodehour->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Smodehour'), ['action' => 'delete', $smodehour->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smodehour->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Smodehours'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Smodehour'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="smodehours view large-10 medium-9 columns">
    <h2><?= h($smodehour->id) ?></h2>
    <div class="row">
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($smodehour->id) ?></p>
            <h6 class="subheader"><?= __('Numberof Hrs') ?></h6>
            <p><?= $this->Number->format($smodehour->numberof_hrs) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Smodes') ?></h4>
    <?php if (!empty($smodehour->smodes)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Mode Name') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($smodehour->smodes as $smodes): ?>
        <tr>
            <td><?= h($smodes->id) ?></td>
            <td><?= h($smodes->mode_name) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Smodes', 'action' => 'view', $smodes->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Smodes', 'action' => 'edit', $smodes->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Smodes', 'action' => 'delete', $smodes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smodes->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
