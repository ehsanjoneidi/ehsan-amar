<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $smode->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $smode->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Smodes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Smodehours'), ['controller' => 'Smodehours', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smodehour'), ['controller' => 'Smodehours', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="smodes form large-10 medium-9 columns">
    <?= $this->Form->create($smode) ?>
    <fieldset>
        <legend><?= __('Edit Smode') ?></legend>
        <?php
            echo $this->Form->input('mode_name');
            echo $this->Form->input('dunits._ids', ['options' => $dunits]);
            echo $this->Form->input('smodehours._ids', ['options' => $smodehours]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
