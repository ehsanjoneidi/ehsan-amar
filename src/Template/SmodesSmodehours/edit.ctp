<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $smodesSmodehour->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $smodesSmodehour->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Smodes Smodehours'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Smodehours'), ['controller' => 'Smodehours', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smodehour'), ['controller' => 'Smodehours', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="smodesSmodehours form large-10 medium-9 columns">
    <?= $this->Form->create($smodesSmodehour) ?>
    <fieldset>
        <legend><?= __('Edit Smodes Smodehour') ?></legend>
        <?php
            echo $this->Form->input('smode_id', ['options' => $smodes, 'empty' => true]);
            echo $this->Form->input('smodehour_id', ['options' => $smodehours, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
