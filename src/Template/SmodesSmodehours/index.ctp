<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Smodes Smodehour'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Smodehours'), ['controller' => 'Smodehours', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Smodehour'), ['controller' => 'Smodehours', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="smodesSmodehours index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('smode_id') ?></th>
            <th><?= $this->Paginator->sort('smodehour_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($smodesSmodehours as $smodesSmodehour): ?>
        <tr>
            <td><?= $this->Number->format($smodesSmodehour->id) ?></td>
            <td>
                <?= $smodesSmodehour->has('smode') ? $this->Html->link($smodesSmodehour->smode->id, ['controller' => 'Smodes', 'action' => 'view', $smodesSmodehour->smode->id]) : '' ?>
            </td>
            <td>
                <?= $smodesSmodehour->has('smodehour') ? $this->Html->link($smodesSmodehour->smodehour->id, ['controller' => 'Smodehours', 'action' => 'view', $smodesSmodehour->smodehour->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $smodesSmodehour->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $smodesSmodehour->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $smodesSmodehour->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smodesSmodehour->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
