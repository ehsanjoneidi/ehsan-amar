<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Smodes Smodehour'), ['action' => 'edit', $smodesSmodehour->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Smodes Smodehour'), ['action' => 'delete', $smodesSmodehour->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smodesSmodehour->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Smodes Smodehours'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Smodes Smodehour'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Smodes'), ['controller' => 'Smodes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Smode'), ['controller' => 'Smodes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Smodehours'), ['controller' => 'Smodehours', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Smodehour'), ['controller' => 'Smodehours', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="smodesSmodehours view large-10 medium-9 columns">
    <h2><?= h($smodesSmodehour->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Smode') ?></h6>
            <p><?= $smodesSmodehour->has('smode') ? $this->Html->link($smodesSmodehour->smode->id, ['controller' => 'Smodes', 'action' => 'view', $smodesSmodehour->smode->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Smodehour') ?></h6>
            <p><?= $smodesSmodehour->has('smodehour') ? $this->Html->link($smodesSmodehour->smodehour->id, ['controller' => 'Smodehours', 'action' => 'view', $smodesSmodehour->smodehour->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($smodesSmodehour->id) ?></p>
        </div>
    </div>
</div>
