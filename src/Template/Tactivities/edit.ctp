<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $tactivity->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $tactivity->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Tactivities'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="tactivities form large-10 medium-9 columns">
    <?= $this->Form->create($tactivity) ?>
    <fieldset>
        <legend><?= __('Edit Tactivity') ?></legend>
        <?php
            echo $this->Form->input('activity_name');
            echo $this->Form->input('dunits._ids', ['options' => $dunits]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
