<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Topics'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Ccontents'), ['controller' => 'Ccontents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ccontent'), ['controller' => 'Ccontents', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="topics form large-10 medium-9 columns">
    <?= $this->Form->create($topic) ?>
    <fieldset>
        <legend><?= __('Add Topic') ?></legend>
        <?php
            echo $this->Form->input('tname');
            echo $this->Form->input('tdescription');
            echo $this->Form->input('ccontent_id', ['options' => $ccontents, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
