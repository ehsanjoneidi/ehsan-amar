<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Ucategory'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dunits'), ['controller' => 'Dunits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dunit'), ['controller' => 'Dunits', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="ucategories index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('ucname') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($ucategories as $ucategory): ?>
        <tr>
            <td><?= $this->Number->format($ucategory->id) ?></td>
            <td><?= h($ucategory->ucname) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $ucategory->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ucategory->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ucategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ucategory->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
