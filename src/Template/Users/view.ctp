<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dcourses'), ['controller' => 'Dcourses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dcourse'), ['controller' => 'Dcourses', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="users view large-10 medium-9 columns">
    <h2><?= h($user->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Username') ?></h6>
            <p><?= h($user->username) ?></p>
            <h6 class="subheader"><?= __('First Name') ?></h6>
            <p><?= h($user->first_name) ?></p>
            <h6 class="subheader"><?= __('Last Name') ?></h6>
            <p><?= h($user->last_name) ?></p>
            <h6 class="subheader"><?= __('User Role') ?></h6>
            <p><?= h($user->user_role) ?></p>
            <h6 class="subheader"><?= __('Password') ?></h6>
            <p><?= h($user->password) ?></p>
            <h6 class="subheader"><?= __('User Email') ?></h6>
            <p><?= h($user->user_email) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($user->id) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Dcourses') ?></h4>
    <?php if (!empty($user->dcourses)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Cname') ?></th>
            <th><?= __('Coverview') ?></th>
            <th><?= __('Cduration') ?></th>
            <th><?= __('Ctype') ?></th>
            <th><?= __('Ccredit') ?></th>
            <th><?= __('Cstatus') ?></th>
            <th><?= __('Qaward Id') ?></th>
            <th><?= __('Isource Id') ?></th>
            <th><?= __('Sarea Id') ?></th>
            <th><?= __('Ccategory Id') ?></th>
            <th><?= __('User Id') ?></th>
            <th><?= __('Dclient Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($user->dcourses as $dcourses): ?>
        <tr>
            <td><?= h($dcourses->id) ?></td>
            <td><?= h($dcourses->cname) ?></td>
            <td><?= h($dcourses->coverview) ?></td>
            <td><?= h($dcourses->cduration) ?></td>
            <td><?= h($dcourses->ctype) ?></td>
            <td><?= h($dcourses->ccredit) ?></td>
            <td><?= h($dcourses->cstatus) ?></td>
            <td><?= h($dcourses->qaward_id) ?></td>
            <td><?= h($dcourses->isource_id) ?></td>
            <td><?= h($dcourses->sarea_id) ?></td>
            <td><?= h($dcourses->ccategory_id) ?></td>
            <td><?= h($dcourses->user_id) ?></td>
            <td><?= h($dcourses->dclient_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Dcourses', 'action' => 'view', $dcourses->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Dcourses', 'action' => 'edit', $dcourses->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dcourses', 'action' => 'delete', $dcourses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dcourses->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
