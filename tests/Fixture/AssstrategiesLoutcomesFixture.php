<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AssstrategiesLoutcomesFixture
 *
 */
class AssstrategiesLoutcomesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'loutcome_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'assstrategy_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'loutcome_id' => ['type' => 'index', 'columns' => ['loutcome_id'], 'length' => []],
            'assstrategy_id' => ['type' => 'index', 'columns' => ['assstrategy_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'assstrategies_loutcomes_ibfk_1' => ['type' => 'foreign', 'columns' => ['loutcome_id'], 'references' => ['loutcomes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'assstrategies_loutcomes_ibfk_2' => ['type' => 'foreign', 'columns' => ['assstrategy_id'], 'references' => ['assstrategies', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'loutcome_id' => 1,
            'assstrategy_id' => 1
        ],
    ];
}
