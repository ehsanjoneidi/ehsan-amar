<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CcontentsCcitationsFixture
 *
 */
class CcontentsCcitationsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'ccontent_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ccitation_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'ccontent_id' => ['type' => 'index', 'columns' => ['ccontent_id'], 'length' => []],
            'ccitation_id' => ['type' => 'index', 'columns' => ['ccitation_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'ccontents_ccitations_ibfk_1' => ['type' => 'foreign', 'columns' => ['ccontent_id'], 'references' => ['ccontents', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'ccontents_ccitations_ibfk_2' => ['type' => 'foreign', 'columns' => ['ccitation_id'], 'references' => ['ccitations', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'ccontent_id' => 1,
            'ccitation_id' => 1
        ],
    ];
}
