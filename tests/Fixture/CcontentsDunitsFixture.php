<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CcontentsDunitsFixture
 *
 */
class CcontentsDunitsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'ccontent_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dunit_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'ccontent_id' => ['type' => 'index', 'columns' => ['ccontent_id'], 'length' => []],
            'dunit_id' => ['type' => 'index', 'columns' => ['dunit_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'ccontents_dunits_ibfk_1' => ['type' => 'foreign', 'columns' => ['ccontent_id'], 'references' => ['ccontents', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'ccontents_dunits_ibfk_2' => ['type' => 'foreign', 'columns' => ['dunit_id'], 'references' => ['dunits', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'ccontent_id' => 1,
            'dunit_id' => 1
        ],
    ];
}
