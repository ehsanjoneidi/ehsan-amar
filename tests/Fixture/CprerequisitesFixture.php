<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CprerequisitesFixture
 *
 */
class CprerequisitesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'ucategoryfor_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'uprerequisite_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'ucategoryfor_id' => ['type' => 'index', 'columns' => ['ucategoryfor_id'], 'length' => []],
            'uprerequisite_id' => ['type' => 'index', 'columns' => ['uprerequisite_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'cprerequisites_ibfk_1' => ['type' => 'foreign', 'columns' => ['ucategoryfor_id'], 'references' => ['ucategories', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'cprerequisites_ibfk_2' => ['type' => 'foreign', 'columns' => ['uprerequisite_id'], 'references' => ['ucategories', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'ucategoryfor_id' => 1,
            'uprerequisite_id' => 1
        ],
    ];
}
