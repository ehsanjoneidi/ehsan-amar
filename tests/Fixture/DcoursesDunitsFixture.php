<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DcoursesDunitsFixture
 *
 */
class DcoursesDunitsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'dcourse_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dunit_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'dcourse_id' => ['type' => 'index', 'columns' => ['dcourse_id'], 'length' => []],
            'dunit_id' => ['type' => 'index', 'columns' => ['dunit_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'dcourses_dunits_ibfk_1' => ['type' => 'foreign', 'columns' => ['dcourse_id'], 'references' => ['dcourses', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dcourses_dunits_ibfk_2' => ['type' => 'foreign', 'columns' => ['dunit_id'], 'references' => ['dunits', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'dcourse_id' => 1,
            'dunit_id' => 1
        ],
    ];
}
