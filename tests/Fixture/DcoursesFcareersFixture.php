<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DcoursesFcareersFixture
 *
 */
class DcoursesFcareersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'dcourse_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'fcareer_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'dcourse_id' => ['type' => 'index', 'columns' => ['dcourse_id'], 'length' => []],
            'fcareer_id' => ['type' => 'index', 'columns' => ['fcareer_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'dcourses_fcareers_ibfk_1' => ['type' => 'foreign', 'columns' => ['dcourse_id'], 'references' => ['dcourses', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dcourses_fcareers_ibfk_2' => ['type' => 'foreign', 'columns' => ['fcareer_id'], 'references' => ['fcareers', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'dcourse_id' => 1,
            'fcareer_id' => 1
        ],
    ];
}
