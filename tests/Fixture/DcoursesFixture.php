<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DcoursesFixture
 *
 */
class DcoursesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'cname' => ['type' => 'string', 'length' => 150, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'coverview' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'cduration' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ctype' => ['type' => 'string', 'length' => 70, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'ccredit' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'cstatus' => ['type' => 'string', 'length' => 70, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'qaward_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'isource_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sarea_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ccategory_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dclient_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'qaward_id' => ['type' => 'index', 'columns' => ['qaward_id'], 'length' => []],
            'isource_id' => ['type' => 'index', 'columns' => ['isource_id'], 'length' => []],
            'sarea_id' => ['type' => 'index', 'columns' => ['sarea_id'], 'length' => []],
            'ccategory_id' => ['type' => 'index', 'columns' => ['ccategory_id'], 'length' => []],
            'user_id' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'dclient_id' => ['type' => 'index', 'columns' => ['dclient_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'dcourses_ibfk_1' => ['type' => 'foreign', 'columns' => ['qaward_id'], 'references' => ['qawards', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dcourses_ibfk_2' => ['type' => 'foreign', 'columns' => ['isource_id'], 'references' => ['isources', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dcourses_ibfk_3' => ['type' => 'foreign', 'columns' => ['sarea_id'], 'references' => ['sareas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dcourses_ibfk_4' => ['type' => 'foreign', 'columns' => ['ccategory_id'], 'references' => ['ccategories', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dcourses_ibfk_5' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dcourses_ibfk_6' => ['type' => 'foreign', 'columns' => ['dclient_id'], 'references' => ['dclients', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'cname' => 'Lorem ipsum dolor sit amet',
            'coverview' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'cduration' => 1,
            'ctype' => 'Lorem ipsum dolor sit amet',
            'ccredit' => 1,
            'cstatus' => 'Lorem ipsum dolor sit amet',
            'qaward_id' => 1,
            'isource_id' => 1,
            'sarea_id' => 1,
            'ccategory_id' => 1,
            'user_id' => 1,
            'dclient_id' => 1
        ],
    ];
}
