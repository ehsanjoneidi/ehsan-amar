<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DcoursesPrecognitionsFixture
 *
 */
class DcoursesPrecognitionsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'dcourse_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'precognition_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'dcourse_id' => ['type' => 'index', 'columns' => ['dcourse_id'], 'length' => []],
            'precognition_id' => ['type' => 'index', 'columns' => ['precognition_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'dcourses_precognitions_ibfk_1' => ['type' => 'foreign', 'columns' => ['dcourse_id'], 'references' => ['precognitions', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dcourses_precognitions_ibfk_2' => ['type' => 'foreign', 'columns' => ['precognition_id'], 'references' => ['dcourses', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'dcourse_id' => 1,
            'precognition_id' => 1
        ],
    ];
}
