<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DunitsFixture
 *
 */
class DunitsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'unit_name' => ['type' => 'string', 'length' => 200, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'unit_type' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'unit_overview' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'unit_credits' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'unit_status' => ['type' => 'string', 'length' => 70, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'updated_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dclient_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'oyear_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'osemester_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ucategory_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'created_id' => ['type' => 'index', 'columns' => ['created_id'], 'length' => []],
            'updated_id' => ['type' => 'index', 'columns' => ['updated_id'], 'length' => []],
            'dclient_id' => ['type' => 'index', 'columns' => ['dclient_id'], 'length' => []],
            'oyear_id' => ['type' => 'index', 'columns' => ['oyear_id'], 'length' => []],
            'osemester_id' => ['type' => 'index', 'columns' => ['osemester_id'], 'length' => []],
            'ucategory_id' => ['type' => 'index', 'columns' => ['ucategory_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'dunits_ibfk_1' => ['type' => 'foreign', 'columns' => ['created_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dunits_ibfk_2' => ['type' => 'foreign', 'columns' => ['updated_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dunits_ibfk_3' => ['type' => 'foreign', 'columns' => ['dclient_id'], 'references' => ['dclients', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dunits_ibfk_4' => ['type' => 'foreign', 'columns' => ['oyear_id'], 'references' => ['oyears', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dunits_ibfk_5' => ['type' => 'foreign', 'columns' => ['osemester_id'], 'references' => ['osemesters', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dunits_ibfk_6' => ['type' => 'foreign', 'columns' => ['ucategory_id'], 'references' => ['ucategories', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'unit_name' => 'Lorem ipsum dolor sit amet',
            'unit_type' => 'Lorem ipsum dolor ',
            'unit_overview' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'unit_credits' => 1,
            'unit_status' => 'Lorem ipsum dolor sit amet',
            'created_id' => 1,
            'updated_id' => 1,
            'dclient_id' => 1,
            'oyear_id' => 1,
            'osemester_id' => 1,
            'ucategory_id' => 1
        ],
    ];
}
