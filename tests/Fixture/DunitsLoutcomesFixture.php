<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DunitsLoutcomesFixture
 *
 */
class DunitsLoutcomesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'dunit_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'loutcome_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'dunit_id' => ['type' => 'index', 'columns' => ['dunit_id'], 'length' => []],
            'loutcome_id' => ['type' => 'index', 'columns' => ['loutcome_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'dunits_loutcomes_ibfk_1' => ['type' => 'foreign', 'columns' => ['dunit_id'], 'references' => ['dunits', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dunits_loutcomes_ibfk_2' => ['type' => 'foreign', 'columns' => ['loutcome_id'], 'references' => ['loutcomes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'dunit_id' => 1,
            'loutcome_id' => 1
        ],
    ];
}
