<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DunitsSmodesFixture
 *
 */
class DunitsSmodesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'dunit_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'smode_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'dunit_id' => ['type' => 'index', 'columns' => ['dunit_id'], 'length' => []],
            'smode_id' => ['type' => 'index', 'columns' => ['smode_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'dunits_smodes_ibfk_1' => ['type' => 'foreign', 'columns' => ['dunit_id'], 'references' => ['dunits', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dunits_smodes_ibfk_2' => ['type' => 'foreign', 'columns' => ['smode_id'], 'references' => ['smodes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'dunit_id' => 1,
            'smode_id' => 1
        ],
    ];
}
