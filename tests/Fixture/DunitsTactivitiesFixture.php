<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DunitsTactivitiesFixture
 *
 */
class DunitsTactivitiesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'tactivity_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dunit_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'tactivity_id' => ['type' => 'index', 'columns' => ['tactivity_id'], 'length' => []],
            'dunit_id' => ['type' => 'index', 'columns' => ['dunit_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'dunits_tactivities_ibfk_1' => ['type' => 'foreign', 'columns' => ['tactivity_id'], 'references' => ['tactivities', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'dunits_tactivities_ibfk_2' => ['type' => 'foreign', 'columns' => ['dunit_id'], 'references' => ['dunits', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'tactivity_id' => 1,
            'dunit_id' => 1
        ],
    ];
}
