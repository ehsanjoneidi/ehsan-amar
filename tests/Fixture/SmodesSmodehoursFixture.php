<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SmodesSmodehoursFixture
 *
 */
class SmodesSmodehoursFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'smode_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'smodehour_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'smode_id' => ['type' => 'index', 'columns' => ['smode_id'], 'length' => []],
            'smodehour_id' => ['type' => 'index', 'columns' => ['smodehour_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'smodes_smodehours_ibfk_1' => ['type' => 'foreign', 'columns' => ['smode_id'], 'references' => ['smodes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'smodes_smodehours_ibfk_2' => ['type' => 'foreign', 'columns' => ['smodehour_id'], 'references' => ['smodehours', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'smode_id' => 1,
            'smodehour_id' => 1
        ],
    ];
}
