<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CcontentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CcontentsTable Test Case
 */
class CcontentsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ccontents',
        'app.topics',
        'app.ccitations',
        'app.ccontents_ccitations',
        'app.dunits',
        'app.ccontents_dunits'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ccontents') ? [] : ['className' => 'App\Model\Table\CcontentsTable'];
        $this->Ccontents = TableRegistry::get('Ccontents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ccontents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
