<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CversionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CversionsTable Test Case
 */
class CversionsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cversions',
        'app.dcourses',
        'app.qawards',
        'app.qlevels',
        'app.isources',
        'app.sareas',
        'app.ccategories',
        'app.users',
        'app.dclients',
        'app.dunits',
        'app.dcourses_dunits',
        'app.fcareers',
        'app.dcourses_fcareers',
        'app.precognitions',
        'app.dcourses_precognitions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Cversions') ? [] : ['className' => 'App\Model\Table\CversionsTable'];
        $this->Cversions = TableRegistry::get('Cversions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cversions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
