<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DcoursesDunitsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DcoursesDunitsTable Test Case
 */
class DcoursesDunitsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dcourses_dunits',
        'app.dcourses',
        'app.qawards',
        'app.qlevels',
        'app.isources',
        'app.sareas',
        'app.ccategories',
        'app.users',
        'app.dclients',
        'app.dunits',
        'app.oyears',
        'app.osemesters',
        'app.ucategories',
        'app.ccontents',
        'app.topics',
        'app.ccitations',
        'app.rcategories',
        'app.ccontents_ccitations',
        'app.ccontents_dunits',
        'app.cresources',
        'app.cresources_dunits',
        'app.loutcomes',
        'app.assstrategies',
        'app.asscategories',
        'app.assstrategies_loutcomes',
        'app.dunits_loutcomes',
        'app.smodes',
        'app.dunits_smodes',
        'app.tactivities',
        'app.dunits_tactivities',
        'app.cversions',
        'app.fcareers',
        'app.dcourses_fcareers',
        'app.precognitions',
        'app.dcourses_precognitions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DcoursesDunits') ? [] : ['className' => 'App\Model\Table\DcoursesDunitsTable'];
        $this->DcoursesDunits = TableRegistry::get('DcoursesDunits', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DcoursesDunits);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
