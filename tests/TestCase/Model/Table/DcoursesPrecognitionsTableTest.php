<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DcoursesPrecognitionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DcoursesPrecognitionsTable Test Case
 */
class DcoursesPrecognitionsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dcourses_precognitions',
        'app.dcourses',
        'app.qawards',
        'app.qlevels',
        'app.isources',
        'app.sareas',
        'app.ccategories',
        'app.users',
        'app.dclients',
        'app.dunits',
        'app.cversions',
        'app.dcourses_dunits',
        'app.fcareers',
        'app.dcourses_fcareers',
        'app.precognitions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DcoursesPrecognitions') ? [] : ['className' => 'App\Model\Table\DcoursesPrecognitionsTable'];
        $this->DcoursesPrecognitions = TableRegistry::get('DcoursesPrecognitions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DcoursesPrecognitions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
