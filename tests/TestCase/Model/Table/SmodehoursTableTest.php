<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SmodehoursTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SmodehoursTable Test Case
 */
class SmodehoursTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.smodehours',
        'app.smodes',
        'app.dunits',
        'app.users',
        'app.dcourses',
        'app.qawards',
        'app.qlevels',
        'app.isources',
        'app.sareas',
        'app.ccategories',
        'app.dclients',
        'app.cversions',
        'app.dcourses_dunits',
        'app.fcareers',
        'app.dcourses_fcareers',
        'app.precognitions',
        'app.dcourses_precognitions',
        'app.oyears',
        'app.osemesters',
        'app.ucategories',
        'app.ccontents',
        'app.topics',
        'app.ccitations',
        'app.rcategories',
        'app.ccontents_ccitations',
        'app.ccontents_dunits',
        'app.cresources',
        'app.cresources_dunits',
        'app.loutcomes',
        'app.assstrategies',
        'app.asscategories',
        'app.assstrategies_loutcomes',
        'app.dunits_loutcomes',
        'app.dunits_smodes',
        'app.tactivities',
        'app.dunits_tactivities',
        'app.smodes_smodehours'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Smodehours') ? [] : ['className' => 'App\Model\Table\SmodehoursTable'];
        $this->Smodehours = TableRegistry::get('Smodehours', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Smodehours);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
