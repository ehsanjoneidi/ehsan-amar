<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SmodesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SmodesTable Test Case
 */
class SmodesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.smodes',
        'app.dunits',
        'app.users',
        'app.dcourses',
        'app.qawards',
        'app.qlevels',
        'app.isources',
        'app.sareas',
        'app.ccategories',
        'app.dclients',
        'app.cversions',
        'app.dcourses_dunits',
        'app.fcareers',
        'app.dcourses_fcareers',
        'app.precognitions',
        'app.dcourses_precognitions',
        'app.oyears',
        'app.osemesters',
        'app.ucategories',
        'app.ccontents',
        'app.topics',
        'app.ccitations',
        'app.rcategories',
        'app.ccontents_ccitations',
        'app.ccontents_dunits',
        'app.cresources',
        'app.cresources_dunits',
        'app.loutcomes',
        'app.assstrategies',
        'app.asscategories',
        'app.assstrategies_loutcomes',
        'app.dunits_loutcomes',
        'app.dunits_smodes',
        'app.tactivities',
        'app.dunits_tactivities',
        'app.smodehours',
        'app.smodes_smodehours'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Smodes') ? [] : ['className' => 'App\Model\Table\SmodesTable'];
        $this->Smodes = TableRegistry::get('Smodes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Smodes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
